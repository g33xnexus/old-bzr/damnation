#!/usr/bin/env python
#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The main application class                                         #
#------------------------------------------------------------------------------#

# Python imports
import sys, os

# Local imports
from Classes.configuration import Configuration


# Create the appropriate game type.
game = None
if Configuration['server'] is not None:
    ## Start multiplayer client ##
    if Configuration['port'] is None:
        print "Error: 'port' must be defined when connecting to a server!"
        sys.exit(1)

    from Classes.GameTypes.client import Client
    game = Client()

elif Configuration['listenPort'] is not None:
    ## Start multiplayer server ##
    if Configuration['dedicated']:
        from pandac.PandaModules import loadPrcFileData
        loadPrcFileData("", "window-type none")

    from Classes.GameTypes.server import Server
    game = Server()

else:
    ## Start single-player game ##
    from Classes.GameTypes.standalone import Standalone
    game = Standalone()


# Panda3D imports
import direct.directbase.DirectStart

game.startLevel()

run()
