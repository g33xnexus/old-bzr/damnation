# Disable v-sync
sync-video 0

# Enable raw mouse input (for multiple mice and >3 buttons)
read-raw-mice #t

# Set up model path
model-path $THIS_PRC_DIR

# FSAA
framebuffer-multisample 1
multisamples 2

#texture-quality-level best

