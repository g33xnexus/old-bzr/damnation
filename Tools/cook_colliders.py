#!/usr/bin/env python

import os
import sys
from optparse import OptionParser


# Parse command line.
parser = OptionParser(usage="%prog [options] input.egg [output.bam]")
parser.add_option("-f", "--collider-format",
        action="store", dest="outputFileFormat", default=None,
        help="the name of the file to write for each collider, relative to the output file's path; use '%s' where 'NODENAME.INDEX' should be added")
parser.add_option("-t", "--triangle",
        action="store_const", const="triangle", dest="collider_type", default="triangle",
        help="use a triangle mesh collider (default)")
parser.add_option("-c", "--convex",
        action="store_const", const="convex", dest="collider_type", default="triangle",
        help="use a convex mesh collider instead of a triangle mesh")
parser.add_option("-n", "--no-split",
        action="store_false", dest="split_meshes", default=True,
        help="don't split each mesh into its own .nxb file")

(options, args) = parser.parse_args()

if len(args) not in range(1,3):
    parser.print_usage()
    sys.exit(1)


# Determine input and output filenames.
inputEgg = os.path.normpath(args[0])

if len(args) > 1:
    outputBam = os.path.normpath(args[1])
else:
    outputBam = inputEgg.replace('.egg', '.bam')

if options.outputFileFormat is not None:
    outputNxbFormat = os.path.join(os.path.dirname(inputEgg), options.outputFileFormat)
else:
    outputNxbFormat = os.path.join(os.path.dirname(inputEgg), '%s.nxb')

if '%s' not in outputNxbFormat:
    print "Collider output filename must contain '%s' where 'NODENAME.INDEX' should be added."
    print outputNxbFormat
    sys.exit(1)


from panda3d.core import loadPrcFileData
 
loadPrcFileData('', 'model-path ' + os.getcwd() + '''
audio-active #f
audio-music-active #f
audio-play-midi #f
audio-play-mp3 #f
audio-play-wave #f''')


import direct.ffi.panda3d
#TODO: Figure out how to stop using this so we don't spawn a blank window for the duration of the conversion.
import direct.directbase.DirectStart

from panda3d.core import Point3, Filename
from panda3d.physx import PhysxKitchen, PhysxTriangleMeshDesc, PhysxConvexMeshDesc, PhysxManager


print "Cooking meshes from '%s' into '%s'. (colliders stored in '%s')" % (inputEgg, outputBam, outputNxbFormat % ('NODENAME.INDEX', ))


# Initialize the engine.
PhysxManager.getGlobalPtr()
kitchen = PhysxKitchen()

# Load the mesh and create the colliders
eggNode = loader.loadModel(inputEgg)

nodeNames = dict()

if options.split_meshes:
    meshesToConvert = eggNode.findAllMatches('**/+CollisionNode')
else:
    meshesToConvert = [eggNode]

for collNode in meshesToConvert:
    nodeName = str(collNode)
    collNode = collNode.getParent()

    if nodeName not in nodeNames:
        index = 1
        nodeNames[nodeName] = index
    else:
        index = nodeNames[nodeName] + 1
        nodeNames[nodeName] = index

    outputFilename = outputNxbFormat % ("%s.%d" % (nodeName.replace('/', '__').replace('\\', '__'), index), )

    # Add collider-loading tag.
    collNode.setTag('colliderFile', '"' + outputFilename + '"')

    if options.collider_type == 'triangle':
        meshDesc = PhysxTriangleMeshDesc()
    elif options.collider_type == 'convex':
        meshDesc = PhysxConvexMeshDesc()

    meshDesc.setFromNodePath(collNode)

    # Cook the egg!
    nxbFilename = Filename.fromOsSpecific(outputFilename)
    if options.collider_type == 'triangle':
        ret = kitchen.cookTriangleMesh(meshDesc, nxbFilename)
    elif options.collider_type == 'convex':
        ret = kitchen.cookConvexMesh(meshDesc, nxbFilename)

    ret = True
    if ret:
        print "Successfully Cooked Mesh", collNode, "to", outputFilename

    else:
        print "Failure! Return code", ret
        sys.exit(2)

eggNode.writeBamFile(outputBam)
print "Finished cooking to '%s'." % (outputBam, )

