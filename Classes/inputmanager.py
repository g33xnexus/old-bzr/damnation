#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The player input class.                                            #
#------------------------------------------------------------------------------#

# Panda imports
from direct.task import Task
from pandac.PandaModules import NodePath, ActorNode, ForceNode, \
    AngularVectorForce, LinearVectorForce, AngularEulerIntegrator

# Python imports
import sys

# Local imports
from configuration import Configuration
from bindings import DigitalSlot, getBinding
from window import Window

from InputDevices.mouse import Mouse
from InputDevices.keyboard import Keyboard
from InputDevices.joystick import Joystick

class InputManager:
    configSection = 'input devices'

    def __init__(self, entity):
        # Set the controlled entity and set the available slots.
        self.setControlledEntity(entity)

        # Find all input devices
        self.devices = Keyboard.getDevices()
        self.devices += Mouse.getDevices()
        self.devices += Joystick.getDevices()

        # Configure devices
        for dev in self.devices:
            self.loadConfiguration(dev)

        for dev in self.devices:
            dev.enableSignals()

        # Add window event handling.
        base.accept("window-event", self.windowEvent)

    def setControlledEntity(self, entity):
        self.controlledEntity = entity

        self.refreshSlots()

    def refreshSlots(self):
        self.slots = {
                'quit': DigitalSlot('quit', self.quitGame),
                'screenshot': DigitalSlot('screenshot', self.screenshot),
                'toggleMouse': DigitalSlot('toggleMouse', Mouse.toggle),
                'toggleFullscreen': DigitalSlot('toggleFullscreen', self.toggleFullscreen),
                }
        for slotName, slot in self.controlledEntity.getSlots().iteritems():
            self.slots[slotName] = slot

    def quitGame(self, value, *args, **kwargs):
        if value:
            print "Screw you guys, I'm going home!"
            sys.exit()

    def screenshot(self, value, *args, **kwargs):
        if value:
            filename = base.screenshot('Damnation')
            print "Screenshot saved in '%s'." % (filename, )

    def toggleFullscreen(self, value, *args, **kwargs):
        if value:
            Window.toggleFullscreen()

    def loadConfiguration(self, device, filename=None):
        if filename is None:
            filename = Configuration[InputManager.configSection, device.name]

        if filename is not None:
            conf = Configuration.loadConfigFile(filename)
            for signalName, bindingString in conf.items("bindings"):
                signal = device.getSignal(signalName)
                if signal is not None:
                    if '#' in bindingString:
                        bindingString = bindingString[:bindingString.find('#')].strip()

                    if "|" not in bindingString:
                        print "Warning: Invalid binding string '%s'! Ignoring." % (bindingString, )
                        continue
                    (bindingName, paramString, slot) = bindingString.split("|", 2)

                    # Check that the slot is valid.
                    if slot not in self.slots:
                        print "Warning: Unrecognized slot '%s'! Ignoring." % (slot, )
                        continue

                    binding = getBinding(bindingName)
                    if binding is None:
                        print "Warning: Unrecognized binding '%s'! Ignoring." % (bindingName, )
                        continue

                    if binding in (signal.getCompatibleBindings() &
                            self.slots[slot].getCompatibleBindings()):
                        # Process binding parameters.
                        params = {}
                        for param in paramString.split(","):
                            if ':' in param:
                                (key, value) = param.split(':', 1)
                                params[key] = float(value)

                        # Bind the signal to the slot with the given binding.
                        signal.setBinding(binding(self.slots[slot],
                            configuration=params))

                    else:
                        print ("Error: Binding type '%s' incompatible with " +
                            "signal '%s' and slot '%s'!") % (bindingName,
                            signalName, slot)

                else:
                    print "Warning: Unrecognized signal '%s'! Ignoring." % (signalName, )

        else:
            print "Warning: No configuration set for device '%s'!" % (device.name, )
            print "Warning: Input device '%s' is connected, " % (device.name, ) + \
                "but has no configuration set!"
            print ("  To add a configuration, add the line '%s=configFile' to" +
                " the [%s] section.") % (device.name, InputManager.configSection)
            device.printInfo()

    def saveConfiguration(self, device, filename):
        #FIXME: Implement this!
        raise "Unimplemented!"

    def windowEvent(self, window, *args, **kwargs):
        if window.getProperties().getForeground():
            print "Debug: Window gained focus. Enabling input."
            for dev in self.devices:
                dev.enableSignals()
        else:
            print "Debug: Window lost focus. Disabling input."
            for dev in self.devices:
                dev.disableSignals()

