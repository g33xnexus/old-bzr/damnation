#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The PhysX world.                                                   #
#------------------------------------------------------------------------------#

__all__ = ["World"]

from pandac.PandaModules import Vec3, Vec4, Mat4, Point3, Filename, \
        AntialiasAttrib

from panda3d.physx import PhysxManager, PhysxSceneDesc, PhysxBodyDesc, \
        PhysxActorDesc, PhysxBoxShapeDesc, PhysxEnums, \
        PhysxCapsuleControllerDesc, PhysxRay

from configuration import Configuration
from util import ColliderUtil


class World(object):
    def __init__(self):
        self.simulationTask = None

        # How often to run the physics simulation, in seconds.
        # Set to 0 for once per frame.
        self.stepSize = float(Configuration['physicsStepSize'])

        # Accumulate time that doesn't line up to the step size so we don't
        # lose simulation steps.
        self.deltaTimeAccumulator = 0

        # Scene
        sceneDesc = PhysxSceneDesc()
        sceneDesc.setGravity(Vec3(0, 0, -98.0665))
        # TODO: Huh?
        sceneDesc.setFlag(PhysxEnums.SFForceConeFriction, True)

        self.scene = PhysxManager.getGlobalPtr().createScene(sceneDesc)
        self.scene.enableControllerReporting(True)
        self.scene.enableContactReporting(True)

        # Collisions
        self.scene.setActorGroupPairFlag(0, 1, PhysxEnums.CPFNotifyOnStartTouch, 1)
        #self.scene.setActorGroupPairFlag(0, 1, PhysxEnums.CPFNotifyOnEndTouch, 1)
        self.scene.setActorGroupPairFlag(0, 1, PhysxEnums.CPFNotifyOnTouch, 1)
        self.scene.setActorGroupPairFlag(1, 1, PhysxEnums.CPFNotifyOnStartTouch, 1)
        #self.scene.setActorGroupPairFlag(1, 1, PhysxEnums.CPFNotifyOnEndTouch, 1)
        self.scene.setActorGroupPairFlag(1, 1, PhysxEnums.CPFNotifyOnTouch, 1)
        self.scene.setActorGroupPairFlag(2, 1, PhysxEnums.CPFNotifyOnStartTouch, 1)
        #self.scene.setActorGroupPairFlag(2, 1, PhysxEnums.CPFNotifyOnEndTouch, 1)
        self.scene.setActorGroupPairFlag(2, 1, PhysxEnums.CPFNotifyOnTouch, 1)

        # Simulation step size
        if self.stepSize > 0:
            self.scene.setTimingFixed(self.stepSize)
        else:
            self.scene.setTimingVariable()

        # Material
        m0 = self.scene.getMaterial(0)
        m0.setRestitution(0.2)
        m0.setStaticFriction(0.3)
        m0.setDynamicFriction(0.3)

        # Entities
        self.entities = dict()
        self.entitiesToLoad = []

        self.sky = None

        self.actor = None
        self.actors = list()
        self.newShapes = list()
        self.actorsToRemove = list()

    def enable(self):
        # Clear the time accumulator.
        self.deltaTimeAccumulator = 0

        # Start the simulation task.
        self.simulationTask = taskMgr.add(self.runSimulation, "Physics Simulation")

        self.enableDebugVisualization()

        render.setAntialias(AntialiasAttrib.MMultisample)

        base.accept('physx-contact-start', self.onContactStart)
        base.accept('physx-contact-stop', self.onContactEnd)
        base.accept('physx-contact-touch', self.onContactContinue)

        print "== Enabled world =="

    def disable(self):
        # Stop the simulation task.
        taskMgr.remove(self.simulationTask)
        self.simulationTask = None

    def setGravity(self, vec):
        self.scene.setGravity(vec)

    def getGravity(self):
        return self.scene.getGravity()

    def finishedLoading(self):
        for shape, node in self.newShapes:
            actorDesc = PhysxActorDesc()
            actorDesc.setName('World')
            actorDesc.addShape(shape)
            actor = self.scene.createActor(actorDesc)
            self.actors.append(actor)
        self.newShapes = list()

    def enableDebugVisualization(self):
        if Configuration.get('debugColliders', '').lower() in ['true', 't', '1', 'enabled', 'on']:
            self.debugNP = render.attachNewNode(self.scene.getDebugGeomNode())
            self.debugNP.node().on()
            self.debugNP.node().visualizeWorldAxes(True)
            self.debugNP.setAntialias(AntialiasAttrib.MNone)

    def loadCollider(self, node, filename):
        print "Loading collider from \"%s\" for %s." % (filename, str(self))
        shapeDesc = ColliderUtil.loadCollider(filename)

        if shapeDesc is not None:
            shapeDesc.setLocalPos(node.getPos(render))
            if self.actor is not None:
                self.actor.createShape(shapeDesc)
            else:
                self.newShapes.append((shapeDesc, node))
        else:
            print "  Couldn't load collider from \"%s\"!" % (filename, )

    def createCollider(self, node, definition):
        if 'type' not in definition or definition['type'] != 'plane':
            print "WARNING: Ignoring createCollider() call. Remove tag 'collider=%s'." % (repr(definition), )
            return

        print "Creating collider \"%s\" for %s." % (definition, str(self))
        shapeDesc = ColliderUtil.createCollider(definition)

        if shapeDesc is not None:
            shapeDesc.setLocalPos(node.getPos(render))
            if self.actor is not None:
                self.actor.createShape(shapeDesc)
            else:
                self.newShapes.append((shapeDesc, node))
        else:
            print "  Error processing collider definition \"%s\"!" % (definition, )

    def runSimulation(self, task):
        for entity in self.entities.values():
            if entity.enabled and 'doMovement' in dir(entity):
                entity.doMovement(self.stepSize)

        dt = globalClock.getDt()
        self.scene.simulate(dt)
        self.scene.fetchResults()

        for entity in self.entities.values():
            if entity.enabled and 'updatePhysics' in dir(entity):
                entity.updatePhysics()

        for actor in self.actorsToRemove:
            try:
                actor.release()
            except AssertionError, e:
                print e
	self.actorsToRemove = list()

        return task.cont

    def removeActor(self, actor):
        self.actorsToRemove.append(actor)

    def onContactStart(self, pair):
        self.callBehaviorMethod(pair.getActorA(), 'collisionStart', pair.getActorB(), pair.getContactPoints())
        self.callBehaviorMethod(pair.getActorB(), 'collisionStart', pair.getActorA(), pair.getContactPoints())

    def onContactEnd(self, pair):
        self.callBehaviorMethod(pair.getActorA(), 'collisionEnd', pair.getActorB(), pair.getContactPoints())
        self.callBehaviorMethod(pair.getActorB(), 'collisionEnd', pair.getActorA(), pair.getContactPoints())

    def onContactContinue(self, pair):
        self.callBehaviorMethod(pair.getActorA(), 'collisionContinue', pair.getActorB(), pair.getContactPoints())
        self.callBehaviorMethod(pair.getActorB(), 'collisionContinue', pair.getActorA(), pair.getContactPoints())

    def callBehaviorMethod(self, actor, methodName, *args):
        if actor is None:
            return None

        behavior = actor.getPythonTag('behavior')
        if behavior is not None:
            method = getattr(behavior, methodName, None)
            if method is not None:
                method(*args)

    def setBehavior(self, actor, behavior):
        actor.setPythonTag('behavior', behavior)

    def getBehavior(self, actor):
        return actor.getPythonTag('behavior')

