#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: Input binding classes.                                             #
#------------------------------------------------------------------------------#

from direct.task.Task import Task


### Key States ###
class KeyState:
    (
        Press,
        Release,
        Repeat
    ) = range(3)



### Signals ###

class Signal(object):
    """The base Signal class for input binding.

    Don't instantiate this directly. Instead, use the Axis or Button
    subclasses.

    """
    def __init__(self, name, description, binding=None):
        self.name = name
        self.description = description
        self.binding = binding

    def __call__(self, value, *args, **kwargs):
        if len(args) > 0 or len(kwargs) > 0:
            print "Warning: Extra args received for signal", self.name

        if self.binding is not None:
            #print "Debug: Signal", self.name, "sending value", value, "to binding", self.binding
            self.binding(value)

    def setBinding(self, binding):
        self.binding = binding


class AxisSignal(Signal):
    def getCompatibleBindings(self):
        return set((AxisSetTo, AxisToggle, AxisMomentary))

class ButtonSignal(Signal):
    def getCompatibleBindings(self):
        return set((ButtonSetTo, ButtonIncrement, ButtonToggle, ButtonMomentary))



### Bindings ###

class Binding(object):
    def __init__(self, target, configuration={}):
        self.target = target
        self.configuration = self.defaultConfig
        for var, value in configuration.iteritems():
            self.setConfiguration(var, value)

    def getConfiguration(self):
        return self.configuration

    def setConfiguration(self, var, value):
        if var in self.configuration:
            self.configuration[var] = value
        else:
            print "Warning: Invalid configuration key '%s'! Ignoring." % (var, )

    def remove(self):
        pass


class AxisSetTo(Binding):
    def __init__(self, target, configuration={}):
        self.defaultConfig = {'deadzone': 0.01, 'offset': 0, 'sensitivity': 0.5}
        Binding.__init__(self, target, configuration)
        target.registerValue(self)
        self.value = 0

    def __call__(self, value):
        self.value = value

    def __str__(self):
        return "AxisSetTo ->" + str(self.target)

    def getValue(self):
        val = (self.value + self.configuration['offset'])
        deadzone = self.configuration['deadzone']
        if val > 0:
            if val < deadzone:
                return 0
            else:
                return self.configuration['sensitivity'] * (val - deadzone)

        else:
            deadzone = -deadzone

            if val > deadzone:
                return 0
            else:
                return self.configuration['sensitivity'] * (val - deadzone)

    def remove(self):
        self.target.unregisterValue(self)

class AxisToggle(Binding):
    def __init__(self, target, configuration={}):
        self.defaultConfig = {'threshold': 0.0}
        Binding.__init__(self, target, configuration)
        self.value = 0
        self.lastValue = 0
        target.registerToggle(self)

    def __call__(self, value):
        self.lastValue = self.value
        self.value = value

    def __str__(self):
        return "AxisToggle ->" + str(self.target)

    def getState(self):
        if self.configuration['invert']:
            return self.value < self.configuration['threshold'] and \
                self.lastValue > self.configuration['threshold']
        else:
            return self.value > self.configuration['threshold'] and \
                self.lastValue < self.configuration['threshold']

class AxisMomentary(Binding):
    def __init__(self, target, configuration={}):
        self.defaultConfig = {'threshold': 0.5, 'invert': False}
        Binding.__init__(self, target, configuration)
        self.value = 0
        target.registerMomentary(self)

    def __call__(self, value):
        self.value = value

    def __str__(self):
        return "AxisMomentary ->" + str(self.target)

    def getState(self):
        if self.configuration['invert']:
            return self.value < self.configuration['threshold']
        else:
            return self.value > self.configuration['threshold']

    def remove(self):
        self.target.unregisterMomentary(self)


class ButtonSetTo(Binding):
    def __init__(self, target, configuration={}):
        self.defaultConfig = {'value': 0.5, 'reset': 0}
        Binding.__init__(self, target, configuration)
        self.pressCount = 0
        if self.configuration['reset'] != 0:
            target.registerDelta(self)
        else:
            target.registerValue(self)

    def __call__(self, value):
        if value == KeyState.Press:
            self.pressCount += 1
        elif value == KeyState.Release:
            self.pressCount -= 1

    def __str__(self):
        return "ButtonSetTo ->" + str(self.target)

    def getReset(self):
        if self.pressCount <= 0:
            self.pressCount = 0
            return False
        else:
            return True

    def getDelta(self):
        return 0

    def getValue(self):
        if self.pressCount <= 0:
            self.pressCount = 0
            return 0
        else:
            return self.configuration['value']

    def remove(self):
        self.target.unregisterValue(self)

class ButtonIncrement(Binding):
    def __init__(self, target, configuration={}):
        self.defaultConfig = {'value': 0.1}
        Binding.__init__(self, target, configuration)
        self.pressCount = 0
        target.registerDelta(self)

    def __call__(self, value):
        if value == KeyState.Press:
            self.pressCount += 1
        elif value == KeyState.Release:
            self.pressCount -= 1

    def __str__(self):
        return "ButtonIncrement ->" + str(self.target)

    def getReset(self):
        return False

    def getDelta(self):
        if self.pressCount <=0:
            self.pressCount = 0
            return 0
        else:
            return self.configuration['value']

    def remove(self):
        self.target.unregisterDelta(self)

class ButtonToggle(Binding):
    def __init__(self, target, configuration={}):
        self.defaultConfig = {}
        Binding.__init__(self, target, configuration)
        self.lastState = False
        target.registerToggle(self)

    def __call__(self, value):
        if value == KeyState.Press:
            self.lastState = not self.lastState

    def __str__(self):
        return "ButtonToggle ->" + str(self.target)

    def getState(self):
        return self.lastState

    def remove(self):
        self.target.unregisterToggle(self)

class ButtonMomentary(Binding):
    def __init__(self, target, configuration={}):
        self.defaultConfig = {}
        Binding.__init__(self, target, configuration)
        self.lastState = False
        target.registerMomentary(self)

    def __call__(self, value):
        if value == KeyState.Press or value == KeyState.Repeat:
            self.lastState = True
        elif value == KeyState.Release:
            self.lastState = False

    def __str__(self):
        return "ButtonMomentary ->" + str(self.target)

    def getState(self):
        return self.lastState

    def remove(self):
        self.target.unregisterMomentary(self)

def getBinding(name):
    bindings = {
        'AxisSetTo': AxisSetTo,
        'AxisToggle': AxisToggle,
        'AxisMomentary': AxisMomentary,
        'ButtonSetTo': ButtonSetTo,
        'ButtonIncrement': ButtonIncrement,
        'ButtonToggle': ButtonToggle,
        'ButtonMomentary': ButtonMomentary,
    }
    if name in bindings:
        return bindings[name]
    else:
        return None



### Slots ###

class Slot(object):
    """The base Slot class for input binding.

    Don't instantiate this directly. Instead, use the Analog or Digital
    subclasses.

    """
    def __init__(self, name, handler=None, description=None, entity=None, *args):
        self.entity = entity
        self.handler = handler
        self.handlerArgs = args
        self.description = description
        self.name = name
        self.lastArgs = []
        self.lastKwargs = {}

    def __call__(self, *args, **kwargs):
        #TODO: Pass the event ID on to the entity creation method when used.
        #if 'eventID' in kwargs:
        #    eventID = kwargs['eventID']
        if self.handler is not None:
            self.handler(*(self.handlerArgs + args), **kwargs)

        from Networking.client import networkClient
        if networkClient is not None and self.entity is not None:
            if self.lastArgs != args or self.lastKwargs != kwargs:
                networkClient.sendEvent(self.entity.ID, self.name, *args, **kwargs)
                self.lastArgs = args
                self.lastKwargs = kwargs


class AnalogSlot(Slot):
    def __init__(self, name, handler=None, description=None, entity=None, posLabel="", negLabel="", *args, **kwargs):
        Slot.__init__(self, name, handler, description, entity, *args, **kwargs)
        self.posLabel = posLabel
        self.negLabel = negLabel
        self.currentThrottle = 0
        self.value = 0
        self.deltaBindings = []
        self.valueBindings = []

        taskMgr.add(self.tskUpdate, "Update analog value.")

    def __str__(self):
        return "AnalogSlot " + self.name

    def __call__(self, value, *args, **kwargs):
        Slot.__call__(self, value, *args, **kwargs)
        self.value = value

    def getCompatibleBindings(self):
        return set((AxisSetTo, ButtonSetTo, ButtonIncrement))

    def reset(self):
        self.currentThrottle = 0

    def registerDelta(self, binding):
        self.deltaBindings.append(binding)

    def unregisterDelta(self, binding):
        self.deltaBindings.remove(binding)

    def registerValue(self, binding):
        self.valueBindings.append(binding)

    def unregisterValue(self, binding):
        self.valueBindings.remove(binding)

    def getValue(self):
        return self.value

    def calculateValue(self):
        dt = globalClock.getDt()
        for binding in self.deltaBindings:
            if binding.getReset():
                self.currentThrottle = 0
                break
            else:
                self.currentThrottle += binding.getDelta() * dt

        value = self.currentThrottle

        for binding in self.valueBindings:
            value += binding.getValue()

        if value > 1.0:
            value = 1.0
        elif value < -1.0:
            value = -1.0

        if self.currentThrottle > 1.0:
            self.currentThrottle = 1.0
        elif self.currentThrottle < -1.0:
            self.currentThrottle = -1.0

        self.value = value

    def tskUpdate(self, task):
        if len(self.deltaBindings) > 0 or len(self.valueBindings) > 0:
            self.calculateValue()
            self(self.getValue())
        return Task.cont


class DigitalSlot(Slot):
    def __init__(self, *args, **kwargs):
        self.repeat = kwargs.pop('repeat', False)

        Slot.__init__(self, *args, **kwargs)

        self.lastState = False
        self.toggleBindings = []
        self.momentaryBindings = []
        self.value = False

        if self.handler is not None:
            taskMgr.add(self.tskUpdate, "Update state.")

    def __str__(self):
        return "DigitalSlot " + self.name

    def __call__(self, value, *args, **kwargs):
        Slot.__call__(self, value, *args, **kwargs)
        self.value = value

    def getCompatibleBindings(self):
        return set((AxisToggle, AxisMomentary, ButtonToggle, ButtonMomentary))

    def registerToggle(self, binding):
        self.toggleBindings.append(binding)

    def unregisterToggle(self, binding):
        self.toggleBindings.remove(binding)

    def registerMomentary(self, binding):
        self.momentaryBindings.append(binding)

    def unregisterMomentary(self, binding):
        self.momentaryBindings.remove(binding)

    def getValue(self):
        return self.value

    def calculateValue(self):
        toggle = False
        for binding in self.toggleBindings:
            toggle = toggle or binding.getState()

        momentary = False
        for binding in self.momentaryBindings:
            momentary = momentary or binding.getState()

        self.value = (momentary and (not toggle)) or ((not momentary) and toggle)

    def tskUpdate(self, task):
        if len(self.toggleBindings) > 0 or len(self.momentaryBindings) > 0:
            oldValue = self.value
            self.calculateValue()
            if self.repeat or oldValue != self.value:
                self(self.getValue())
        return Task.cont

