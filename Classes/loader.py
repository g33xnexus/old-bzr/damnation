#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The egg loader.                                                    #
#------------------------------------------------------------------------------#

__all__ = ["ContentLoader"]

import os.path, re, random

from direct.task import Task
from pandac.PandaModules import CollisionSphere, CollisionNode, NodePath, \
        Filename, VirtualFileSystem, PointLight, Spotlight, DirectionalLight, \
        AmbientLight, Vec3, Vec4, Point3, OmniBoundingVolume, VBase4, \
        StringStream, CollisionTraverser, TexturePool, Texture
from pandac.extension_native_helpers import *

import Entities
from world import World
from configuration import Configuration


class ContentLoader(object):
    def __init__(self):
        self.entityCache = dict()

        self.world = World()

        self.anisotropicDegree = int(Configuration.get('anisotropicDegree', 1))

        tmf = Configuration.get('textureMinFilter', 'FTLinearMipmapLinear')
        if tmf == 'FTNearest':
            self.textureMinFilter = Texture.FTNearest
        elif tmf == 'FTLinear':
            self.textureMinFilter = Texture.FTLinear
        elif tmf == 'FTNearestMipmapNearest':
            self.textureMinFilter = Texture.FTNearestMipmapNearest
        elif tmf == 'FTLinearMipmapNearest':
            self.textureMinFilter = Texture.FTLinearMipmapNearest
        elif tmf == 'FTNearestMipmapLinear':
            self.textureMinFilter = Texture.FTNearestMipmapLinear
        elif tmf == 'FTLinearMipmapLinear':
            self.textureMinFilter = Texture.FTLinearMipmapLinear

        tmf = Configuration.get('textureMagFilter', 'FTLinear')
        if tmf == 'FTNearest':
            self.textureMagFilter = Texture.FTNearest
        elif tmf == 'FTLinear':
            self.textureMagFilter = Texture.FTLinear
        elif tmf == 'FTNearestMipmapNearest':
            self.textureMagFilter = Texture.FTNearestMipmapNearest
        elif tmf == 'FTLinearMipmapNearest':
            self.textureMagFilter = Texture.FTLinearMipmapNearest
        elif tmf == 'FTNearestMipmapLinear':
            self.textureMagFilter = Texture.FTNearestMipmapLinear
        elif tmf == 'FTLinearMipmapLinear':
            self.textureMagFilter = Texture.FTLinearMipmapLinear

    def onCollision(self, entry):
        #TODO: Remove!

        #print "loader.onCollision"
        #print entry.getBody1(), entry.getBody2()
        #if not entry.getBody1().isEmpty():
        #    print entry.getBody1().getData()
        #if not entry.getBody2().isEmpty():
        #    print entry.getBody2().getData()

        if entry.getBody1().isEmpty() or entry.getBody2().isEmpty():
            return

        thisCollision = frozenset((entry.getBody1().getData(), entry.getBody2().getData()))

        if thisCollision not in self.collisionsThisFrame:
            self.collisionsThisFrame.add(thisCollision)

            contact = entry.getContactPoint(0)
            velocity1 = entry.getBody1().getPointVel(contact)
            velocity2 = entry.getBody2().getPointVel(contact)

            normal = entry.getContactGeom(0).getNormal()
            netVelocity = (velocity1 - velocity2).project(normal).length()

            # Extremely rough kinetic energy approximation
            kineticEnergy = netVelocity * entry.getBody1().getMass().getMagnitude() / 4

            # Make the damage playable
            kineticDamage = kineticEnergy / 30000

            #print "Collision! ", entry.getBody1().getData(), entry.getBody1().getLinearVel(), entry.getBody2().getData(), "", entry.getBody2().getLinearVel(), " (%f damage)" % kineticDamage

            if 'doDamage' in dir(entry.getBody1().getData()):
                entry.getBody1().getData().doDamage({'kinetic': kineticDamage})

            if 'doDamage' in dir(entry.getBody2().getData()):
                entry.getBody2().getData().doDamage({'kinetic': kineticDamage})

            if 'onCollision' in dir(entry.getBody1().getData()):
                entry.getBody1().getData().onCollision(entry.getBody2().getData())

            if 'onCollision' in dir(entry.getBody2().getData()):
                entry.getBody2().getData().onCollision(entry.getBody1().getData())

    def getWorld(self):
        return self.world

    def loadEntitiesFromDefNodes(self, parentNode):
        for entityDefNode in parentNode.findAllMatches("**/=entity"):
            class entityCallback:
                def __init__(self, placeholder):
                    self.placeholder = placeholder

                def __call__(self, *entities):
                    if len(entities) > 1:
                        print "WARNING: More than one entity in callback! Ignoring all but the first."
                    ent = entities[0].node
                    ent.setName(self.placeholder.getName())
#                    parent = self.placeholder.getParent()
#                    ent.reparentTo(parent)

                    # FIXME: WHY IS THIS BROKEN?
                    #ent.setPos(self.placeholder.getPos())
                    #ent.setHpr(self.placeholder.getHpr())

                    self.placeholder.removeNode()

            entityDef = entityDefNode.getTagAsPythonTag("entity")

            if 'position' not in entityDef:
                pos = entityDefNode.getPos()
                entityDef['position'] = [pos[0], pos[1], pos[2]]

            if 'hpr' not in entityDef:
                hpr = entityDefNode.getHpr()
                entityDef['hpr'] = [hpr[0], hpr[1], hpr[2]]

            filename = entityDef.pop("filename")
            self.world.entitiesToLoad.append(filename)

            print "    Loading entity", entityDefNode.getName(), "from", filename
            self.loadEntityEgg(filename, parentNode, None, entityDef, entityCallback(entityDefNode), loadSubEntities=True)

    def loadLevelEgg(self,
            filename,
            parent=render,
            name=None):
        # TODO: Add int position and scale, and turn this into loadZoneFile?
        if name is None:
            name = os.path.basename(filename)

        print ""
        print "Loading level", filename, "inside", parent

        node = loader.loadModel(filename)

        self.updateTextureParameters()

        print "  Loading entities..."
        self.loadEntitiesFromDefNodes(node)

        print "  Loading lights..."
        for light in node.findAllMatches("**/=light"):
            print "    Loading light", light.getName()
            lightDef = light.getTagAsPythonTag("light")
            if lightDef['lightType'] == 'directional':
                lightNode = DirectionalLight('lightNode')
            elif lightDef['lightType'] == 'ambient':
                lightNode = AmbientLight('lightNode')

            if 'color' in lightDef:
                lightNode.setColor(VBase4(*lightDef['color']))
            if 'specularColor' in lightDef:
                lightNode.setSpecularColor(VBase4(*lightDef['specularColor']))
            lightNodeAttached = render.attachNewNode(lightNode)
            if 'orientation' in lightDef:
                lightNodeAttached.setHpr(*lightDef['orientation'])
            render.setLight(lightNodeAttached)

        for model in node.findAllMatches("**/=noShader"):
            if model.getTagAsBool("noShader"):
                if model is None or "setShaderAuto" not in dir(model):
                    raise Exception("ContentLoader: 'noShader' key used on " + \
                        "incompatible node: " + str(model))
                print "Setting noShader."
                model.setShaderOff()
                model.clearTag("noShader")

        for model in node.findAllMatches("**/=scale"):
            if model is None or "setScale" not in dir(model):
                raise Exception("ContentLoader: 'scale' key used on " + \
                    "incompatible node: " + str(model))
            model.setScale(model.getTagAsPythonTag("scale"))
            model.clearTag("scale")

        for model in node.findAllMatches("**/=compass"):
            if model.getTagAsBool("compass"):
                if model is None or "setCompass" not in dir(model):
                    raise Exception("ContentLoader: 'compass' key used on " + \
                        "incompatible node: " + str(model))
                print "Setting compass."
                model.setCompass(render)
                model.clearTag("compass")

        for model in node.findAllMatches("**/=bin"):
            if model is None or "setBin" not in dir(model):
                raise Exception("ContentLoader: 'bin' key used on " + \
                    "incompatible node: " + str(model))
            print "Setting bin", model.getTagAsPythonTag("bin")
            model.setBin(model.getTagAsPythonTag("bin"), 1)
            model.clearTag("bin")

        for model in node.findAllMatches("**/=depthWrite"):
            if model is None or "setDepthWrite" not in dir(model):
                raise Exception("ContentLoader: 'depthWrite' key used on " + \
                    "incompatible node: " + str(model))
            print "Setting depthWrite", model.getTagAsPythonTag("depthWrite")
            model.setDepthWrite(model.getTagAsBool("depthWrite"))
            model.clearTag("depthWrite")

        for model in node.findAllMatches("**/=depthTest"):
            if model is None or "setDepthTest" not in dir(model):
                raise Exception("ContentLoader: 'depthTest' key used on " + \
                    "incompatible node: " + str(model))
            print "Setting depthTest", model.getTagAsPythonTag("depthTest")
            model.setDepthTest(model.getTagAsBool("depthTest"))
            model.clearTag("depthTest")

        for model in node.findAllMatches("**/=bounds"):
            if model is None or "node" not in dir(model):
                raise Exception("ContentLoader: 'bounds' key used on " + \
                    "incompatible node: " + str(model))
            print "Setting bounds", model.getTagAsPythonTag("bounds")
            if model.getTagAsPythonTag("bounds") == "omni":
                model.node().setBounds(OmniBoundingVolume())
            else:
                raise Exception("ContentLoader: Unrecognized bounding volume: %s" \
                    % (value))
            model.clearTag("bounds")

        for model in node.findAllMatches("**/=final"):
            if model is None or "node" not in dir(model):
                raise Exception("ContentLoader: 'final' key used on " + \
                    "incompatible node: " + str(model))
            print "Setting final", model.getTagAsPythonTag("final")
            model.node().setFinal(model.getTagAsBool("final"))
            model.clearTag("final")

        for skydef in node.findAllMatches("**/=sky"):
            if self.world.sky is not None:
                print "Sky already set! Ignoring duplicate 'sky' tag on '%s'." % (str(skydef), )
            else:
                skyName = skydef.getTagAsPythonTag("sky")
                print "Loading sky '%s' (tag on '%s')" % (skyName, str(skydef))
                self.world.sky = loader.loadModel(skyName)
                skydef.clearTag("sky")
                self.world.sky.setBin('background', 1) 
                self.world.sky.setDepthWrite(0) 
                self.world.sky.reparentTo(base.camera) 
                self.world.sky.setCompass() 


        if base.camera is not None:
            for model in node.findAllMatches("**/=cameraRelative"):
                if model.getTagAsBool("cameraRelative"):
                    if model is None or "reparentTo" not in dir(model):
                        raise Exception("ContentLoader: 'cameraRelative' key used on " + \
                            "incompatible node: " + str(model))
                    print "Setting cameraRelative."
                    model.reparentTo(base.camera)
                    model.clearTag("cameraRelative")

        for model in node.findAllMatches("**/=colliderFile"):
            self.world.loadCollider(model, model.getTagAsPythonTag("colliderFile"))
            model.clearTag("colliderFile")

        for model in node.findAllMatches("**/=collider"):
            self.world.createCollider(model, model.getTagAsPythonTag("collider"))
            model.clearTag("collider")

        node.reparentTo(parent)

        self.world.finishedLoading()

        print "Level load finished."
        return node

    def loadEntityEgg(self,
            filename,
            parent=render,
            name=None,
            state=None,
            callback=None,
            loadSubEntities=False):
        if name is None:
            name = os.path.basename(filename)

        print "Loading entity from", filename

        if callback is not None:
            job = loader.loadModel(filename, callback=self.finishLoadEntityEggCallback(self, filename, callback, parent, state))
        else:
            node = loader.loadModel(filename)
            return self.finishLoadEntityEgg(node, filename, parent, state, loadSubEntities)

    def finishLoadEntityEgg(self, loadedNode, filename, parent, state=None, loadSubEntities=False):
        outermost = None

        self.updateTextureParameters()

        if loadedNode is None:
            print "Error loading entity '%s'!" % (filename, )
            return

        for node in loadedNode.findAllMatches("**/=behavior"):
            if parent is not None:
                # FIXME: Handle this failure currectly:
                # "AssertionError: other._error_type == ET_ok at line 462 of panda/src/pgraph/nodePath.cxx"
                try:
                    node.reparentTo(parent)
                except:
                    pass

            node.flattenStrong()
            node.setShaderAuto()

            for model in node.findAllMatches("**/=noShader"):
                if model.getTagAsBool("noShader"):
                    if model is None or "setShaderAuto" not in dir(model):
                        raise Exception("ContentLoader: 'noShader' key used on " + \
                            "incompatible node: " + str(model))
                    print "Setting noShader."
                    model.setShaderOff()
                    model.clearTag("noShader")

            for model in node.findAllMatches("**/=scale"):
                if model is None or "setScale" not in dir(model):
                    raise Exception("ContentLoader: 'scale' key used on " + \
                        "incompatible node: " + str(model))
                model.setScale(model.getTagAsPythonTag("scale"))
                model.clearTag("scale")
                model.flattenLight()

            for model in node.findAllMatches("**/=compass"):
                if model.getTagAsBool("compass"):
                    if model is None or "setCompass" not in dir(model):
                        raise Exception("ContentLoader: 'compass' key used on " + \
                            "incompatible node: " + str(model))
                    print "Setting compass."
                    model.setCompass(render)
                    model.clearTag("compass")

            for model in node.findAllMatches("**/=bin"):
                if model is None or "setBin" not in dir(model):
                    raise Exception("ContentLoader: 'bin' key used on " + \
                        "incompatible node: " + str(model))
                print "Setting bin", model.getTagAsPythonTag("bin")
                model.setBin(model.getTagAsPythonTag("bin"), 1)
                model.clearTag("bin")

            for model in node.findAllMatches("**/=depthWrite"):
                if model is None or "setDepthWrite" not in dir(model):
                    raise Exception("ContentLoader: 'depthWrite' key used on " + \
                        "incompatible node: " + str(model))
                print "Setting depthWrite", model.getTagAsPythonTag("depthWrite")
                model.setDepthWrite(model.getTagAsBool("depthWrite"))
                model.clearTag("depthWrite")

            for model in node.findAllMatches("**/=depthTest"):
                if model is None or "setDepthTest" not in dir(model):
                    raise Exception("ContentLoader: 'depthTest' key used on " + \
                        "incompatible node: " + str(model))
                print "Setting depthTest", model.getTagAsPythonTag("depthTest")
                model.setDepthTest(model.getTagAsBool("depthTest"))
                model.clearTag("depthTest")

            for model in node.findAllMatches("**/=bounds"):
                if model is None or "node" not in dir(model):
                    raise Exception("ContentLoader: 'bounds' key used on " + \
                        "incompatible node: " + str(model))
                print "Setting bounds", model.getTagAsPythonTag("bounds")
                if model.getTagAsPythonTag("bounds") == "omni":
                    model.node().setBounds(OmniBoundingVolume())
                else:
                    raise Exception("ContentLoader: Unrecognized bounding volume: %s" \
                        % (value))
                model.clearTag("bounds")

            for model in node.findAllMatches("**/=final"):
                if model is None or "node" not in dir(model):
                    raise Exception("ContentLoader: 'final' key used on " + \
                        "incompatible node: " + str(model))
                print "Setting final", model.getTagAsPythonTag("final")
                model.node().setFinal(model.getTagAsBool("final"))
                model.clearTag("final")

            if base.camera is not None:
                for model in node.findAllMatches("**/=cameraRelative"):
                    if model.getTagAsBool("cameraRelative"):
                        if model is None or "reparentTo" not in dir(model):
                            raise Exception("ContentLoader: 'cameraRelative' key used on " + \
                                "incompatible node: " + str(model))
                        print "Setting cameraRelative."
                        model.reparentTo(base.camera)
                        model.clearTag("cameraRelative")

            entity = None

            if node.hasTag("behavior"):
                behaviorName = node.getTag("behavior")
                entity = Entities.getEntityClass(behaviorName)(node)
                node.setPythonTag("entity", entity)
                self.world.entities[entity.ID] = entity

                if state is not None:
                    entity.setState(state)

                entity.definitionFilename = None
                if outermost is None:
                    if filename is not None:
                        entity.definitionFilename = filename
                    outermost = entity
            else:
                print "WARNING: No 'behavior' key in entity definition! " + \
                    "Returning raw node for", node.getName()
                if outermost is None:
                    outermost = node


            if entity is not None:
                for model in node.findAllMatches("**/=colliderFile"):
                    entity.loadCollider(model.getTagAsPythonTag("colliderFile"))
                    model.clearTag("colliderFile")

                for model in node.findAllMatches("**/=collider"):
                    entity.createCollider(model.getTagAsPythonTag("collider"))
                    model.clearTag("collider")


            if loadSubEntities is True:
                print "  Loading sub-entities..."
                self.loadEntitiesFromDefNodes(node)


            if entity is not None:
                entity.finishedLoading()


        print "Finished loading entity '%s'." % (filename, )
        if filename in self.world.entitiesToLoad:
            self.world.entitiesToLoad.remove(filename)
            self.enableSimIfLoadFinished()
        return outermost

    class finishLoadEntityEggCallback:
        def __init__(self, contentloader, filename, callback, parent, state=None, loadSubEntities=False):
            self.contentloader = contentloader
            self.filename = filename
            self.callback = callback
            self.parent = parent
            self.state = state
            self.loadSubEntities = loadSubEntities

        def __call__(self, *nodes):
            finished = list()
            for node in nodes:
                finished.append(self.contentloader.finishLoadEntityEgg(node, self.filename, self.parent, self.state, self.loadSubEntities))
            self.callback(*finished)

    def updateTextureParameters(self):
        for texture in TexturePool.findAllTextures():
            if texture.getMagfilter() != self.textureMagFilter \
                    or texture.getMinfilter() != self.textureMinFilter \
                    or texture.getAnisotropicDegree() != self.anisotropicDegree:
                texture.setMagfilter(self.textureMagFilter)
                texture.setMinfilter(self.textureMinFilter)
                texture.setAnisotropicDegree(self.anisotropicDegree)
                print "Updated attributes on texture:", texture

    def getEntities(self):
        return self.world.entities.values()

    def getEntityByID(self, eid):
        if eid not in self.world.entities:
            raise "Request for unknown entity ID!"
        return self.world.entities[eid]

    def getStartPosition(self):
        startPosCollection = render.findAllMatches("**/=player_start")
        startPositions = [startPosCollection[x] for x in range(startPosCollection.getNumPaths())]

        if len(startPositions) == 0:
            return None

        startPos = random.choice(startPositions)
        startDef = startPos.getTagAsPythonTag("player_start")

        if startDef is not None and 'position' in startDef:
            pos = startDef["position"]
        elif startDef is not None and 'pos' in startDef:
            pos = startDef["pos"]
        else:
            pos = startPos.getPos()
            pos = [pos[0], pos[1], pos[2]]

        if startDef is not None and 'orientation' in startDef:
            hpr = startDef["orientation"]
        elif startDef is not None and 'hpr' in startDef:
            hpr = startDef["hpr"]
        else:
            hpr = startPos.getHpr()
            hpr = [hpr[0], hpr[1], hpr[2]]

        return (pos, hpr)

    def deleteEntity(self, entity):
        if entity.ID in self.world.entities:
            del self.world.entities[entity.ID]
            entity.remove()
            #TODO: Make sure the behavior is actually disabled and all that shit.

    def enableSimIfLoadFinished(self):
        if len(self.world.entitiesToLoad) == 0:
            # Start physics simulation.
            print "Finished loading all queued entities; enabling simulation."
            self.world.enable()



floatRegex = re.compile(r'^-?[0-9]*\.[0-9]*$')
def pythonTagFromEgg(string):

    def getElements(string):
        groups = {
            "(": ")",
            "[": "]",
            "{": "}",
            "\"": "\"",
        }
        elements = string.split(',')
        output = []
        idx = 0
        while idx < len(elements):
            if len(elements[idx]) > 0:
                openChar = elements[idx][0]
                start = idx
                if openChar not in groups and ':' in elements[idx]:
                    openChar = elements[idx].split(':')[1].strip()[0]
                if openChar not in groups and '=' in elements[idx]:
                    openChar = elements[idx].split('=')[1].strip()[0]
                if openChar in groups:
                    closeChar = groups[openChar]
                    while elements[idx][-1] != closeChar:
                        idx += 1
                    output.append(",".join(elements[start:idx + 1]).strip())
                else:
                    output.append(elements[idx].strip())
            idx += 1
        return output

    string = string.strip()

    if len(string) == 0:
        return None

    elif string[0] == '{' and string[-1] == '}':
        # Encoded dict
        elements = getElements(string[1:-1])
        newdict = dict()
        for e in elements:
            if ':' in e:
                (key, value) = e.split(':', 1)
            elif '=' in e:
                (key, value) = e.split('=', 1)
            else:
                print "WARNING: No ':' or '=' in dict element '%s'!" % (e, )
                continue
            newdict[key.strip()] = pythonTagFromEgg(value.strip())
        return newdict

    elif string[0] in ('(', '[') and string[-1] in (')', ']'):
        # Encoded list
        elements = getElements(string[1:-1])
        newlist = list()
        for e in elements:
            newlist.append(pythonTagFromEgg(e.strip()))
        return newlist

    elif string[0] in ("'", "\"") and string[-1] in ("'", "\""):
        # String value
        return string[1:-1]

    elif (string[0] == "-" or string[0].isdigit()) and (len(string) == 1 or string[1:].isdigit()):
        return int(string)

    elif floatRegex.match(string):
        return float(string)

    else:
        print "WARNING: Couldn't parse value '%s'; returning as string." % (string, )
        return string

def pythonTagToEgg(value):
    if isinstance(value, dict):
        # dict
        return "{" + ", ".join([repr(key) + ": " + pythonTagToEgg(value[key]) for key in value.keys()]) + "}"
    elif isinstance(value, list):
        # list
        return "[" + ", ".join([pythonTagToEgg(item) for item in value]) + "]"
    elif isinstance(value, tuple):
        # tuple
        return "[" + ", ".join([pythonTagToEgg(item) for item in value]) + "]"
    elif isinstance(value, str) or isinstance(value, unicode):
        # string
        return repr(str(value))
    else:
        return repr(value)

def getTagAsPythonTag(self, tagname):
    # Get the tag 'tagname' and parse it into a python datatype
    return pythonTagFromEgg(self.getTag(tagname))
Dtool_funcToMethod(getTagAsPythonTag, NodePath)
del getTagAsPythonTag

def setTagAsPythonTag(self, tagname, value):
    # Convert Python data to a string representation and store in the given tag
    self.setTag(tagname, pythonTagToEgg(value))
Dtool_funcToMethod(setTagAsPythonTag, NodePath)
del setTagAsPythonTag

def getTagAsBool(self, tagname):
    return self.hasTag(tagname) and self.getTag(tagname).lower() not in ("false", "f", "0")
Dtool_funcToMethod(getTagAsBool, NodePath)
del getTagAsBool


ContentLoader = ContentLoader()

for cls in Entities.entityClasses.values():
    cls.contentLoader = ContentLoader

