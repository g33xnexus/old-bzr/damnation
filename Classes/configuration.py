#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The configuration class                                            #
#------------------------------------------------------------------------------#

# Python imports
import sys, os, ConfigParser, hashlib, random
from optparse import OptionParser

# Local imports
from version import VERSION

class Configuration(object):
    userConfigDir = os.path.expanduser('~/.config/damnation')

    def __init__(self):
        # Set up command-line parser.
        self.optionParser = OptionParser(version="%prog " + VERSION)
        self.optionParser.add_option("-d", "--debug", action="store_true",
            dest="debugMode", help="enable debug mode")

        self.optionParser.add_option("-f", "--config-file", action="store",
            dest="configFile", help="load the configuration file FILE",
            metavar="FILE")

        self.optionParser.add_option("-s", "--enable-pstats", action="store_true",
            dest="enablePStats", help="enable pstats mode")

        self.optionParser.add_option("-c", "--connect", action="store",
            type="string", dest="server", help="server to connect to",
            metavar="SERVER")

        self.optionParser.add_option("-p", "--port", action="store",
            type="int", dest="port", help="port to connect to server on",
            metavar="PORT")

        (self.options, self.cmdArgs) = self.optionParser.parse_args()

        if len(self.cmdArgs) > 0:
            print "Positional arguments? Oh noes! I DON'T KNOW WHAT TO DO!!!!1one."

        # Set up configuration parser.
        if self.options.configFile is not None:
            self.config = self.loadConfigFile('damnation.cfg', self.options.configFile)
        else:
            self.config = self.loadConfigFile('damnation.cfg')

        # Set default values for options in the 'general' section.
        self.defaults = {
            'port': 2695,
        }

    def __getitem__(self, key):
        return self.get(key)

    def get(self, key, default=None):
        if type(key) == type(tuple()):
            if self.config.has_option(*key):
                return self.config.get(*key)
            #TODO: Defaults? Maybe not...
            else:
                return default
        else:
            if getattr(self.options, key, None) is not None:
                return getattr(self.options, key)
            elif self.config.has_option('general', key):
                return self.config.get('general', key)
            elif key in self.defaults:
                return self.defaults[key]
            else:
                return default

    def getBool(self, key, default=False):
        value = self.get(key, False)
        if isinstance(value, str):
            return value.lower() in ['t', 'true', 'yes', 'on', 'enable', 'enabled']
        else:
            return value in [True, 1]

    def items(self, *args, **kwargs):
        return self.config.items(*args, **kwargs)

    def has_section(self, *args, **kwargs):
        return self.config.has_section(*args, **kwargs)

    def loadConfigFile(self, filename, *args):
        filenames = [os.path.expanduser(filename)]

        if not os.path.isabs(filename):
            # Try relative to the user's config dir first.
            user = os.path.join(self.userConfigDir, filename)
            filenames.insert(0, user)

            # Then try in the Config/ subdir.
            configdir = os.path.join('Config', filename)
            filenames.insert(0, configdir)

        # Add other manually-added filenames.
        filenames = list(args) + filenames

        confParser = ConfigParser.SafeConfigParser()
        print "Loading configuration files:", str(confParser.read(filenames))[1:-1]
        return confParser

Configuration = Configuration()

