#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The connected client class for the server.                         #
#------------------------------------------------------------------------------#

# Panda imports
from direct.fsm import FSM
from direct.distributed.PyDatagram import PyDatagram

# Python imports
import new

# Local imports
from connection import Connection, Messages
from Classes.version import VERSION
from Classes.loader import ContentLoader

class ConnectedClient(Connection, FSM.FSM):
    lastClientID = 0

    def __init__(self,
			connection,
			server,
			compress=False,
			reader=None,
			writer=None,
			manager=None,
			udp=None,
            serverUdpPort=None):
        Connection.__init__(self, compress, reader, writer, manager)
        FSM.FSM.__init__(self, 'connectionFSM')
        self.defaultTransitions = {
            'Incoming' : ['Connected', 'Disconnected'],
            'Connected' : ['Active', 'Disconnected'],
            'Active' : ['Disconnected'],
            'Disconnected' : [],
            }

        self.server = server
        self.udp = udp
        self.serverUdpPort = serverUdpPort

        ConnectedClient.lastClientID += 1
        self.clientID = ConnectedClient.lastClientID

        self.setConnection(connection)

        self.clientUdpAddress = None
        self.request('Incoming') # Switch to the 'Incoming' state.

        self.controlledEntity = None

    def enterIncoming(self):
        print str(self.clientID) + ": ->Incoming"
        def processIncoming(self, data):
            # When receiving 'connect' message, request the Connected state.
            if data[0] == Messages.Connect:
                #TODO: Make version check a bit more correct.
                if data[1] < VERSION:
                    # This client's version is too old; disconnect.
                    print "Disconnecting client %d with old version %s." % (self.clientID, data[1])
                    self.request('Disconnected')
                    return
                #TODO: Keep track of other clientish things.
                self.clientUdpAddress = self.connection.getAddress()
                self.clientUdpAddress.setPort(data[2])
                self.server.setClientUdpAddress(self, self.clientUdpAddress)

                self.request('Connected')

        self.process = new.instancemethod(processIncoming, self, self.__class__)

    def exitIncoming(self):
        print str(self.clientID) + ": Incoming->"

    def enterConnected(self):
        print str(self.clientID) + ": ->Connected"
        def processConnected(self, data):
            # When receiving 'character info' message, request the Ready state.
            if data[0] == Messages.CharacterInfo:
                self.request('Active')
                #TODO: Store character info.

        self.process = new.instancemethod(processConnected, self, self.__class__)

        def processUdp(self, data):
            if data[0] == Messages.Ping:
                datagram = PyDatagram()
                datagram.addString(self.encode(
                        [Messages.Pong, data[1], globalClock.getFrameTime()],
                        self.compress))
                self.sendUnreliable(datagram)

        self.processUdp = new.instancemethod(processUdp, self, self.__class__)

        self.sendData([Messages.ConnectionAccepted, self.clientID, self.serverUdpPort])

    def exitConnected(self):
        print str(self.clientID) + ": Connected->"

    def enterActive(self):
        print str(self.clientID) + ": ->Active"
        def processActive(self, data):
            # When receiving 'event' message, pass to the appropriate entity.
            if data[0] == Messages.Event:
                #print "Debug: Received event %s from client %s." % (data[1:], self.clientID)
                entity = ContentLoader.getEntityByID(data[1])
                (event, args, kwargs, eventID) = data[2:6]
                entity.event(event, eventID=eventID, *args, **kwargs)
            elif data[0] == Messages.UnknownEntity:
                try:
                    entity = ContentLoader.getEntityByID(data[1])
                    self.sendEntity(entity)
                except:
                    pass

        self.process = new.instancemethod(processActive, self, self.__class__)

        #TODO: Figure this out from the character info.
        definitionFile = Configuration['playerEntityFile']

        #TODO: Get this from the filesystem or hash cache or something.
        definitionHash = 0

        startpos = ContentLoader.getStartPosition()

        if startpos is None:
            print "No start position found for client %d!" % (self.clientID, )
            return

        state = {
            'position': startpos[0],
            'hpr': startpos[1],
        }
        if definitionFile.endswith(".egg"):
            playerEntity = ContentLoader.loadEntityEgg(
                filename=definitionFile,
                state=state, loadSubEntities=True)
        else:
            playerEntity = ContentLoader.loadEntityFile(
                filename=definitionFile,
                state=state)

        self.controlledEntity = playerEntity

        self.sendData([Messages.ControlledEntity,
            playerEntity.ID,
            state,
            definitionFile,
            definitionHash])

        self.server.createEntity(playerEntity, skipClient=self)

        print "#### New Client ####"
        print "### Sending Entities ###"
        for entity in ContentLoader.getEntities():
            print entity
            if entity.ID != playerEntity.ID:
                self.sendEntity(entity)

        print "### Sending Entity States ###"
        for entity in ContentLoader.getEntities():
            print entity
            state = entity.getState()
            self.sendState(entity, state, True)

    def exitActive(self):
        print str(self.clientID) + ": Active->"

    def enterDisconnected(self):
        print str(self.clientID) + ": ->Disconnected"
        # Clean up connection.

        def processDisconnected(self, data):
            raise "Received data on a closed connection?"

        self.process = new.instancemethod(processDisconnected, self, self.__class__)

        self.server.deleteEntity(self.controlledEntity)

    def exitDisconnected(self):
        print str(self.clientID) + ": Disconnected->"

    def sendUnreliable(self, datagram):
        self.writer.send(datagram, self.udp, self.clientUdpAddress)

    def sendState(self, behavior, state, force=False):
        if self.state != 'Active' and not force:
            print "Debug: Ignoring state update while client is not in 'Active' state; current state:", self.state
            return

        print "Debug: Sending entity state %s to client %s." % (state, self.clientID)
        self.sendData([Messages.UpdateEntity, behavior.ID, state, globalClock.getFrameTime()])

    def sendEntity(self, behavior):
        if behavior is None:
            raise "Tried to send None as an entity!"
        #print "Debug: Sending behavior:", behavior
        definitionFilename = behavior.definitionFilename
        #TODO: Get this from the filesystem or hash cache or something.
        definitionHash = 0
        self.sendData([Messages.CreateEntity,
            behavior.ID,
            {},
            definitionFilename,
            definitionHash])

    def deleteEntity(self, behavior):
        if behavior is None:
            raise "Tried to delete a None entity!"
        #print "Debug: Deleting entity:", behavior
        self.sendData([Messages.DeleteEntity,
            behavior.ID])

