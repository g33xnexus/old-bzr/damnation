#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The basic server class.                                            #
#------------------------------------------------------------------------------#

# Panda imports
from pandac.PandaModules import QueuedConnectionManager, \
    QueuedConnectionListener, QueuedConnectionReader, ConnectionWriter, \
    PointerToConnection, NetAddress, NetDatagram
from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator
from direct.task.Task import Task


# Create client-side entity reference encode/decode
from types import IntType
from struct import pack, unpack
import rencode, Classes.hooks


class EntityEncoder(object):
    def __init__(self, cls):
        self.cls = cls

    def __call__(self, entity):
        #TODO: Redo using Python Long to raise our max. entities.
        return "%s%s" % (rencode.protocol[self.cls], pack("!I", entity.ID))


class EntityDecoder(object):
    def __init__(self, cls):
        self.cls = cls

    def __call__(self, data):
        #TODO: Redo using Python Long to raise our max. entities.
        #TODO: Add handling for incorrect entity IDs.
        entityID = unpack('!I', data.read(4))[0]
        return ContentLoader.getEntityByID(entityID)


registeredEntityClasses = 0
entityClassProtocol = [chr(i) for i in range(33, 256) if chr(i) not in rencode.protocol.values()]

def registerEntityClass(cls):
    global registeredEntityClasses, entityClassProtocol
    rencode.protocol[cls] = entityClassProtocol[registeredEntityClasses]
    rencode.register_encoder_for_type(cls)(EntityEncoder(cls))
    rencode.register_decoder_for_type(cls)(EntityDecoder(cls))
    registeredEntityClasses += 1
    return cls

Classes.hooks.entity.registerHook(registerEntityClass)


# Local imports
from connection import Connection, Messages
from connectedclient import ConnectedClient
from Classes.loader import ContentLoader


class Server:
    def __init__(self, port, backlog=1000, compress=True):
        self.port = int(port)
        self.backlog = int(backlog)
        self.compress = compress

        self.connectionManager = QueuedConnectionManager()
        self.connectionListener = QueuedConnectionListener(self.connectionManager, 0)
        self.connectionWriter = ConnectionWriter(self.connectionManager,0)

        # Open UDP port
        self.udpPort = 49200
        self.udp = self.connectionManager.openUDPConnection(self.udpPort)
        if self.udp is None:
            print "Warning: Couldn't open UDP connection on default port " + \
                "'%d'! Trying fallback ports." % (self.udpPort, )
        while self.udp is None:
            self.udpPort += 1
            if self.udpPort > 65535:
                raise "Couldn't open a UDP port between 49200 and 65535!"
            self.udp = self.connectionManager.openUDPConnection(self.udpPort)
        print "UDP connection opened on port", self.udpPort
        self.udpConnectionReader = QueuedConnectionReader(self.connectionManager, 0)
        self.udpConnectionReader.addConnection(self.udp)

        self.activeConnections = []
        self.clientUdpAddresses = {}

        if port is not None:
            # Listen on the given port
            self.listen()

    def listen(self):
        # Bind to our socket
        tcpSocket = self.connectionManager.openTCPServerRendezvous(self.port, self.backlog)
        self.connectionListener.addConnection(tcpSocket)
        self.startPolling()

    def startPolling(self):
        taskMgr.add(self.tskListenerPolling, "serverListenTask", -40)
        taskMgr.add(self.tskDisconnectPolling, "serverDisconnectTask", -39)
        taskMgr.add(self.tskProcessUdp, "serverUdpTask", -41)
        taskMgr.doMethodLater(0.02, self.sendStateUpdates, "Send entity state updates to all clients")

    def tskListenerPolling(self, task):
        "Listen for incoming connections."
        if self.connectionListener.newConnectionAvailable():
            rendezvous = PointerToConnection()
            netAddress = NetAddress()
            newConnection = PointerToConnection()

            if self.connectionListener.getNewConnection(rendezvous, netAddress, newConnection):
                newConnection = newConnection.p()

                # Create a new ConnectedClient instance for the incoming connection.
                self.activeConnections.append(
                    ConnectedClient(
                        newConnection,
                        server=self,
                        compress=self.compress,
                        writer=self.connectionWriter,
                        manager=self.connectionManager,
                        udp=self.udp,
                        serverUdpPort=self.udpPort))

        return Task.cont

    def tskDisconnectPolling(self, task):
        "Check for disconnected connections."
        while self.connectionManager.resetConnectionAvailable() == True:
            connPointer = PointerToConnection()
            self.connectionManager.getResetConnection(connPointer)
            connection = connPointer.p()

            # Loop through the activeConnections till we find the connection we just deleted
            # and remove it from our activeConnections list
            for c in range(0, len(self.activeConnections)):
                if self.activeConnections[c].connection == connection:
                    self.activeConnections[c].request('Disconnected')
                    self.activeConnections[c].setConnection(None)
                    del self.activeConnections[c]
                    break

        return Task.cont

    def getClients(self):
        # return a list of all activeConnections
        return self.activeConnections

    def broadcastData(self, data):
        myPyDatagram = PyDatagram()
        myPyDatagram.addString(Connection.encode(data, self.compress))
        for client in self.activeConnections:
            self.connectionWriter.send(myPyDatagram, client.connection)

    def createEntity(self, entity, skipClient=None):
        state = entity.getState()
        for client in self.activeConnections:
            if client != skipClient:
                client.sendEntity(entity)
                client.sendState(entity, state, True)

    def deleteEntity(self, entity, skipClient=None):
        ContentLoader.deleteEntity(entity)
        for client in self.activeConnections:
            if client != skipClient:
                client.deleteEntity(entity)

    def sendStateUpdates(self, task):
        for entity in ContentLoader.getEntities():
            state = entity.getStateDelta()

            # Don't send empty state updates.
            if len(state) > 0:
                # Create the datagram for this state update.
                datagram = PyDatagram()
                datagram.addString(Connection.encode([
                        Messages.UpdateEntity,
                        entity.ID,
                        state,
                        globalClock.getFrameTime()],
                    self.compress))

                if not self.connectionWriter.isValidForUdp(datagram):
                    #FIXME: Figure out how to split up the state dict in this case.
                    raise "Tried to send a state update that's too big!"

                clients = []
                for client in self.activeConnections:
                    # Only send updates if the client is active.
                    if client.state == 'Active':
                        #TODO: Change this to only send this entity's update if the
                        # given player is close enough.
                        clients.append(client.clientID)
                        client.sendUnreliable(datagram)
                #print "Debug: Sent entity state %s to clients: %s" % (state, str(clients))

        return Task.again

    def tskProcessUdp(self, task):
        while self.udpConnectionReader.dataAvailable():
            datagram = NetDatagram()
            # Check if we got a valid datagram:
            if self.udpConnectionReader.getData(datagram):
                clientAddress = datagram.getAddress()
                clientAddress = (clientAddress.getIpString(), clientAddress.getPort())
                if clientAddress in self.clientUdpAddresses:
                    datagramIterator = PyDatagramIterator(datagram)
                    self.clientUdpAddresses[clientAddress].processUdp(
                        Connection.decode(datagramIterator.getString()))
                else:
                    print "Warning: UDP packet received from unknown address/port:", clientAddress

        return Task.cont

    def setClientUdpAddress(self, client, address):
        self.clientUdpAddresses[address.getIpString(), address.getPort()] = client

