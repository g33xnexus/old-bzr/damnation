#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The basic client class.                                            #
#------------------------------------------------------------------------------#

# Panda imports
from pandac.PandaModules import QueuedConnectionManager, \
    QueuedConnectionListener, QueuedConnectionReader, ConnectionWriter, \
    NetAddress, NetDatagram, PointerToConnection
from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator
from direct.task.Task import Task
from direct.fsm import FSM

# Python imports
import sys, os, new


# Create client-side entity reference encode/decode
from types import IntType
from struct import pack, unpack
import rencode, Classes.hooks


class EntityEncoder(object):
    def __init__(self, cls):
        self.cls = cls

    def __call__(self, entity):
        #TODO: Redo using Python Long to raise our max. entities.
        #TODO: Add handling for unknown network entity IDs.
        return "%s%s" % (rencode.protocol[self.cls], pack("!I", networkClient.entityLocalIDToNetID[entity.ID]))


class EntityDecoder(object):
    def __init__(self, cls):
        self.cls = cls

    def __call__(self, data):
        #TODO: Redo using Python Long to raise our max. entities.
        #TODO: Add handling for unknown network entity IDs.
        entityNetID = unpack('!I', data.read(4))[0]
        if entityNetID in networkClient.entityNetIDToLocal:
            return networkClient.entityNetIDToLocal[entityNetID]
        else:
            print "WARNING: Unknown network entity ID '%d'" % (entityNetID)
            return None


registeredEntityClasses = 0
entityClassProtocol = [chr(i) for i in range(33, 256) if chr(i) not in rencode.protocol.values()]

def registerEntityClass(cls):
    global registeredEntityClasses, entityClassProtocol
    rencode.protocol[cls] = entityClassProtocol[registeredEntityClasses]
    rencode.register_encoder_for_type(cls)(EntityEncoder(cls))
    rencode.register_decoder_for_type(cls)(EntityDecoder(cls))
    registeredEntityClasses += 1
    return cls

Classes.hooks.entity.registerHook(registerEntityClass)


# Local imports
from connection import Connection, Messages
from Classes.version import VERSION
from Classes.loader import ContentLoader

networkClient = None


class Client(Connection, FSM.FSM):
    def __init__(self,
            charInfo,
            host=None,
            port=None,
            timeout=3000,
            compress=True,
            setControlledEntity=None):
        self.cManager = QueuedConnectionManager()
        self.udpreader = None
        self.serverUdpAddress = None
        self.stateUpdates = {}

        global networkClient
        networkClient = self

        Connection.__init__(self, compress, manager=self.cManager)
        FSM.FSM.__init__(self, 'clientFSM')
        self.defaultTransitions = {
            'Initialized' : ['Connected', 'Disconnected'],
            'Connected' : ['Active', 'Disconnected'],
            'Active' : ['Disconnected'],
            'Disconnected' : [],
            }

        self.pingTimeout = timeout
        self.charInfo = charInfo
        self.setControlledEntity = setControlledEntity

        self.clientID = None
        self.lastEventID = 0

        self.pingTimeout = 10
        self.lastPingID = 0
        self.pings = {}
        self.timestampOffset = 0

        # A map from network entity ID to behavior object
        self.entityNetIDToLocal = {}
        self.entityLocalIDToNetID = {}

        self.host = host
        self.port = port
        self.pingTimeout = timeout

        if host is not None:
            self.request('Initialized', host, port)

    def enterInitialized(self, host, port):
        self.host = host
        self.port = port

        if self.port is None:
            self.port = 2695

        # Connect to our host
        print "Connecting to %s on port %d..." % (self.host, self.port)
        if not self.connect():
            raise Exception("Couldn't connect to %s port %d!" % (self.host, self.port))

        # Open UDP port
        self.udpPort = 49200
        self.udp = self.cManager.openUDPConnection(self.udpPort)
        if self.udp is None:
            print "Warning: Couldn't open UDP connection on default port " + \
                "'%d'! Trying fallback ports." % (self.udpPort, )
        while self.udp is None:
            self.udpPort += 1
            if self.udpPort > 65535:
                raise Exception("Couldn't open a UDP port between 49200 and 65535!")
            self.udp = self.cManager.openUDPConnection(self.udpPort)
        print "UDP connection opened on port", self.udpPort

        def processInitialized(self, data):
            if data[0] == Messages.ConnectionAccepted:
                # TODO: Warn if too many data elements, error if too few.
                self.clientID = data[1]

                self.serverUdpAddress = NetAddress()
                self.serverUdpAddress.setHost(self.host, data[2])

                self.request('Connected')

        self.process = new.instancemethod(processInitialized, self, self.__class__)

        # Send our connection message
        self.sendData([Messages.Connect, VERSION, self.udpPort]) # Send connect packet.

    def exitInitialized(self):
        pass

    def enterConnected(self):
        print "Connected."

        def processConnected(self, data):
            if data[0] == Messages.ControlledEntity:
                # TODO: Warn if too many data elements, error if too few.
                entity = self.loadEntityFromPacket(allowInstancing=False, *data[1:5])

                if self.setControlledEntity is not None:
                    self.setControlledEntity(entity)

                self.request('Active')

            else:
                #TODO: Otherwise, queue up packets and process when we enter Active.
                pass

        self.process = new.instancemethod(processConnected, self, self.__class__)

        self.sendPing()
        taskMgr.doMethodLater(1, self.sendPing, "Send pings to determine clock offset.")

        self.sendData([Messages.CharacterInfo, self.charInfo]) # Send character info packet.

    def exitConnected(self):
        pass

    def enterActive(self):
        print "Ready."
        def processActive(self, data):
            messageID = data[0]
            if messageID == Messages.CreateEntity:
                # TODO: Warn if too many data elements, error if too few.
                self.loadEntityFromPacket(*data[1:5])
            elif messageID == Messages.UpdateEntity:
                # TODO: Warn if too many data elements, error if too few.
                self.handleUpdateEntity(*data[1:4])
            elif messageID == Messages.DeleteEntity:
                self.deleteEntity(data[1])

        self.udpreader = QueuedConnectionReader(self.cManager, 0)
        self.udpreader.addConnection(self.udp)

        #TODO: Process queued packets by passing them all to processActive.
        self.process = new.instancemethod(processActive, self, self.__class__)

    def exitActive(self):
        pass

    def enterDisconnected(self, reason=None):
        #TODO: Give user feedback for disconnected state.

        def processDisconnected(self, data):
            raise Exception("Received data on a closed connection?")

        self.process = new.instancemethod(processDisconnected, self, self.__class__)
        self.udpreader = None

    def exitDisconnected(self):
        pass

    def processUdp(self, data):
        messageID = data[0]
        if messageID == Messages.UpdateEntity:
            # TODO: Warn if too many data elements, error if too few.
            self.handleUpdateEntity(*data[1:4])

        elif messageID == Messages.Pong:
            if data[1] in self.pings:
                #print "Debug: Received Pong %d with timestamp %.6f" % (data[1], data[2])
                localTime = globalClock.getFrameTime()
                rtt = localTime - self.pings[data[1]]
                self.timestampOffset = localTime - data[2] - (rtt / 2)
                for pingnum in range(data[1], -1, -1):
                    if pingnum in self.pings:
                        if pingnum != data[1]:
                            print "Ping %d was dropped; newer pong received." % pingnum
                        del self.pings[pingnum]
                    else:
                        break
            else:
                print "Warning: Ignoring extra Pong %d!" % (data[1], )

    def finishUdp(self):
        #if len(self.stateUpdates) > 0:
        #    print "Debug: Applying state updates:", self.stateUpdates
        leftovers = dict()
        for entityNetID, data in self.stateUpdates.iteritems():
            (state, timestamp) = data
            #TODO: Change ALL messages to use entities directly in the message instead of the entity ID.
            if entityNetID not in self.entityNetIDToLocal:
                print "Warning: Unknown network entity ID", entityNetID
                self.sendData([Messages.UnknownEntity, entityNetID])
                leftovers[entityNetID] = data
            else:
                self.entityNetIDToLocal[entityNetID].setState(state,
                        timestamp + self.timestampOffset)
        self.stateUpdates = leftovers

    def tskProcessData(self, task):
        "Process incoming data."
        if not self.connected:
            return Task.done

        while self.reader.dataAvailable():
            datagram = NetDatagram()
            # Check if we got a valid datagram:
            if self.reader.getData(datagram):
                datagramIterator = PyDatagramIterator(datagram)
                self.process(self.decode(datagramIterator.getString()))

        if self.udpreader is not None:
            while self.udpreader.dataAvailable():
                datagram = NetDatagram()
                # Check if we got a valid datagram:
                if self.udpreader.getData(datagram):
                    serverAddress = datagram.getAddress()
                    serverAddress = (serverAddress.getIpString(), serverAddress.getPort())
                    if serverAddress != self.serverUdpAddress:
                        datagramIterator = PyDatagramIterator(datagram)
                        self.processUdp(self.decode(datagramIterator.getString()))
                    else:
                        print "Warning: UDP packet received from unknown address/port:", serverAddress
            self.finishUdp()

        return Task.cont

    def loadEntityFromPacket(
            self,
            entityNetID,
            entityState,
            entityFile,
            entityHash,
            allowInstancing=True):
        #TODO: Check if filename exists and hash is correct, and download if
        # needed.
        if entityFile.endswith(".egg"):
            entity = ContentLoader.loadEntityEgg(
                entityFile,
                state=entityState)
        else:
            entity = ContentLoader.loadEntityFile(
                entityFile,
                state=entityState,
                allowInstancing=allowInstancing)
        self.entityNetIDToLocal[entityNetID] = entity
        self.entityLocalIDToNetID[entity.ID] = entityNetID

        return entity

    def handleUpdateEntity(self, entityNetID, state, timestamp):
        # Only retain this state update if it's newer than the latest one
        # for this entity.
        if (entityNetID not in self.stateUpdates) or (timestamp >
                self.stateUpdates[entityNetID][1]):
            if entityNetID in self.stateUpdates:
                tempState = self.stateUpdates[entityNetID][0]
                tempState.update(state)
                state = tempState
            self.stateUpdates[entityNetID] = (state, timestamp)

    def deleteEntity(self, entityNetID):
        entity = self.entityNetIDToLocal[entityNetID]
        del self.entityNetIDToLocal[entityNetID]
        del self.entityLocalIDToNetID[entity.ID]

    def connect(self):
        # Connect to our host's socket
        self.setConnection(self.cManager.openTCPClientConnection(
            self.host,
            self.port,
            self.pingTimeout
        ))
        if self.connected:
            self.startPolling() # Start polling to see if we lost connection
        return self.connected

    def startPolling(self):
        taskMgr.add(self.tskDisconnectPolling, "clientDisconnectTask", -39)

    def tskDisconnectPolling(self, task):
        while self.cManager.resetConnectionAvailable() == True:
            connPointer = PointerToConnection()
            self.cManager.getResetConnection(connPointer)
            connection = connPointer.p()

            if connection != self.connection:
                raise Exception("Closing a connection different than the one that's open?")

            # Remove the connection we just found to be "reset" or "disconnected"
            self.setConnection(None)
            return Task.done

        return Task.cont

    def sendPing(self, task=None):
        if len(self.pings) > self.pingTimeout:
            raise Exception("Ping timed out!")
        self.lastPingID += 1
        self.pings[self.lastPingID] = globalClock.getFrameTime()
        #print "Debug: Sending ping %d." % (self.lastPingID, )
        self.sendUnreliable([Messages.Ping, self.lastPingID]) # Send ping packet.

        return Task.again

    def sendUnreliable(self, data):
        datagram = PyDatagram()
        datagram.addString(self.encode(data, self.compress))
        self.writer.send(datagram, self.udp, self.serverUdpAddress)

    def sendEvent(self, entityID, eventName, *args, **kwargs):
        if entityID not in self.entityLocalIDToNetID:
            raise Exception("Unknown network ID for local entity %d!" % (entityID, ))
        self.lastEventID += 1
        self.sendData([Messages.Event, self.entityLocalIDToNetID[entityID], eventName, args, kwargs, self.lastEventID])
        return self.lastEventID

