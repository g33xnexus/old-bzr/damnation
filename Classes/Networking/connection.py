#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The base class for all connections.                                #
#------------------------------------------------------------------------------#

# Panda imports
from pandac.PandaModules import QueuedConnectionReader, ConnectionWriter, \
    NetDatagram
from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator
from direct.task.Task import Task

# Local imports
import rencode

class Messages:
    (
        # Messages for TCP:
        Connect, # [Connect, clientVersion, udpPort]
        ConnectionAccepted, # [ConnectionAccepted, clientID]
        CharacterInfo, # [CharacterInfo, charInfo]
        ControlledEntity, # [ControlledEntity, entityID, state, definitionFile, definitionHash]
        CreateEntity, # [CreateEntity, entityID, state, definitionFilename, definitionHash]
        ReplaceEntity, # [ReplaceEntity, entityID, state, definitionFilename, definitionHash]
        DeleteEntity, # [DeleteEntity, entityID]
        UpdateEntity, # [UpdateEntity, entityID, state, timestamp]
        UnknownEntity, # [UnknownEntity, entityID]

        # Messages for UDP:
        Event, # [Event, entityID, event, args, kwargs, eventID, timestamp]
        Ping, # [Ping, pingID]
        Pong, # [Pong, pingID, timestamp]
    ) = range(12)

class Connection(object):
    @staticmethod
    def encode(data, compress=True):
        "Encode the given data for transmission over the network."
        #TODO: Integrate PyDatagram instantiation into this.
        return rencode.dumps(data, compress)

    @staticmethod
    def decode(data):
        "Decode the given network data."
        #TODO: Integrate PyDatagram reading into this.
        return rencode.loads(data)


    def __init__(self, compress=True, reader=None, writer=None, manager=None):
        self.compress = compress
        self.connection = None
        self.connected = False

        self.reader = reader
        if self.reader is None:
            if manager is None:
                raise "Expected either reader or manager when initializing connection!"
            self.reader = QueuedConnectionReader(manager, 0)

        self.writer = writer
        if self.writer is None:
            if manager is None:
                raise "Expected either writer or manager when initializing connection!"
            self.writer = ConnectionWriter(manager,0)

    def setConnection(self, connection):
        if self.connection:
            # Remove our previous connection from the reader and writer.
            self.reader.removeConnection(self.connection)

        self.connection = connection
        if self.connection:
            # Set our new connection and add it to the reader and writer.
            self.reader.addConnection(self.connection)
            self.connected = True

            # Set up a task to read incoming packets.
            taskMgr.add(self.tskProcessData, "connectionProcessTask", -40)

        else:
            # We're disconnected!
            self.connected = False

    def process(self, data):
        raise "Connection.process() should be overridden!"

    def sendData(self, data):
        "Send the given data over this connection."
        datagram = PyDatagram()
        datagram.addString(self.encode(data, self.compress))
        if (self.connection == None):
            #TODO: We need to notify the state machine, and move to Disconnected
            self.connected = False
        else:
            self.writer.send(datagram, self.connection)

    def tskProcessData(self, task):
        "Process incoming data."
        if not self.connected:
            return Task.done

        while self.reader.dataAvailable():
            datagram = NetDatagram()
            # Check if we got a valid datagram:
            if self.reader.getData(datagram):
                datagramIterator = PyDatagramIterator(datagram)
                self.process(self.decode(datagramIterator.getString()))
        return Task.cont

