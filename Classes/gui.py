#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Ryan D. Wilson <ryan.wilson@g33xnexus.com>                        #
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The GUI class.                                                     #
#------------------------------------------------------------------------------#

from direct.gui.OnscreenText import OnscreenText
from direct.gui.DirectGui import DirectWaitBar
from direct.task import Task
from pandac.PandaModules import GraphicsEngine, Vec4

import overlays


class Gui:
    def __init__(self, player):
        self.player = player

        self.panelPadding = (2, 2, 2, 2)
        self.itemMargin = 2

        self.base = overlays.PixelNode('base')

        self.hudFont = overlays.TextOverlay.loadFont('Fonts/antigravbb_reg.otf', size=16)
        self.hudColor = Vec4(1, 1, 1, 1)


        self.velocityPanel = overlays.OverlayContainer('velocityPanel')
        self.velocityPanel.reparentTo(self.base)

        self.velocityTextX = overlays.TextOverlay(msg="Lateral: 0.0 m/s      ",
                font=self.hudFont, color=self.hudColor)
        self.velocityTextX.reparentTo(self.velocityPanel)
        self.velocityTextX.setPos(0, 0)

        self.velocityTextY = overlays.TextOverlay(msg="Forward: 0.0 m/s      ",
                font=self.hudFont, color=self.hudColor)
        self.velocityTextY.reparentTo(self.velocityPanel)
        self.velocityTextY.setPos(0, 0)

        self.velocityTextZ = overlays.TextOverlay(msg="Vertical: 0.0 m/s      ",
                font=self.hudFont, color=self.hudColor)
        self.velocityTextZ.reparentTo(self.velocityPanel)
        self.velocityTextZ.setPos(0, 0)


        self.damagePanel = overlays.OverlayContainer('damagePanel')
        self.damagePanel.reparentTo(self.base)
        self.damagePanel.setPos(50, 100)

        self.damageTextHull = overlays.TextOverlay(msg="Hull:     0    ",
                font=self.hudFont, color=self.hudColor)
        self.damageTextHull.reparentTo(self.damagePanel)
        self.damageTextHull.setPos(0, 0)

        """
        # Throttle bars
        self.throttleBar = DirectWaitBar(text = "THROTTLE", value = 0,
            pos = (-1.02, -0.97, -0.96), scale = 0.3, range = 1,
            barColor = (0.0, 0.0, 1.0, 0.8), text_fg = (1.0, 1.0, 1.0, 1.0),
            frameColor = (0.7, 0.7, 0.7, 0.5))
        self.throttleReverseBar = DirectWaitBar(text = "REVERSE THROTTLE", 
            value = 0, pos = (-1.02, -0.97, -0.90), scale = 0.3,
            range = 1, barColor = (1.0, 0.0, 0.0, 0.8),
            text_fg = (1.0, 1.0, 1.0, 1.0), frameColor = (0.7, 0.7, 0.7, 0.5))
        self.hullBar = DirectWaitBar(text = "HULL", 
            value = 1.0, pos = (1.02, 0.97, -0.96), scale = 0.3,
            range = 1, barColor = (0.1, 0.1, 0.1, 0.95),
            text_fg = (1.0, 1.0, 1.0, 1.0), frameColor = (0.8, 0.0, 0.0, 0.8))
        """
		
        base.accept('aspectRatioChanged', self.base.aspectRatioChanged)

        self.updateValues()

        taskMgr.doMethodLater(0.02, self.updateValues, "Update the throttle/velocity values")

    def updateVelocityPanel(self):
        # Update text
        velocity = self.player.controlledEntity.getVelocity()
        textX = "Lateral: %.1f m/s" % (velocity[0])
        self.velocityTextX.setText(textX)
        textY = "Forward: %.1f m/s" % (velocity[1])
        self.velocityTextY.setText(textY)
        textZ = "Vertical: %.1f m/s" % (velocity[2])
        self.velocityTextZ.setText(textZ)

        # Update size and position of panel elements.
        width = self.panelPadding[0]
        height = self.panelPadding[1]

        for text in (self.velocityTextX, self.velocityTextY, self.velocityTextZ):
            textWidth, textHeight = text.getSize()
            text.setPos(self.panelPadding[0], height)
            width = max(textWidth + self.panelPadding[0] + self.panelPadding[2], width)
            height += textHeight + self.itemMargin

        width += self.panelPadding[2]
        height += self.panelPadding[3] - self.itemMargin

        self.velocityPanel.setPos(5, base.win.getYSize() - height - 5)

    def updateDamagePanel(self):
        # Update text
        hull = self.player.controlledEntity.strength.get()
        textHull = "Hull:    %d" % (hull)
        self.damageTextHull.setText(textHull)

        #hullPercent = hull / self.player.controlledEntity.maxStrength.get()
        #self.hullBar['value'] = max(0, hullPercent)

        # Update size and position of panel elements.
        width = self.panelPadding[0]
        height = self.panelPadding[1]

        textWidth, textHeight = self.damageTextHull.getSize()
        self.damageTextHull.setPos(self.panelPadding[0], height)
        width = max(textWidth + self.panelPadding[0] + self.panelPadding[2], width)
        height += textHeight

        width += self.panelPadding[2]
        height += self.panelPadding[3]

        self.damagePanel.setPos(base.win.getXSize() - width - 5, base.win.getYSize() - height - 5)

    def displayThrottle(self):
        throttle = self.player.controlledEntity.getThrottle('throttle')
        self.throttleBar['value'] = max(0, throttle)
        self.throttleReverseBar['value'] = max(-throttle, 0)

    def updateValues(self, task=None):
        #self.displayThrottle()
        self.updateDamagePanel()
        self.updateVelocityPanel()
        return Task.again

