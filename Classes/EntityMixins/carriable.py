#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The carriable entity class                                         #
#------------------------------------------------------------------------------#

__all__ = ["Carriable"]

from Classes.hooks import entity_mixin
from physical import Physical


@entity_mixin
class Carriable(object):
    mixin_dependencies = [Physical]

    def __entity_init__(self, node):
        self.handles = self.createProperty('handles', [
                # (start_x, start_y, start_z, end_x, end_y, end_z, radius)
                (-1, 0, 0, 1, 0, 0, 0.5)
                ])
        self.bulk = self.createProperty('bulk', 1.0) # in liters (cubic decimeters)
        self.size = self.createProperty('size', 1.0) # in decimeters

    def _entity_setState(self, state, timestamp=None):
        """Set this entity's state.

        state should be a dict, where keys correspond to individual state
            elements.

        """
        self.handles.setFromState(state, timestamp)
        self.bulk.setFromState(state, timestamp)
        self.size.setFromState(state, timestamp)

    def _entity_getState(self):
        """Get this entity's state.

        Returns a dict, where keys correspond to individual state elements.

        """
        self.handles.addToState(state)
        self.bulk.addToState(state)
        self.size.addToState(state)

        return state

