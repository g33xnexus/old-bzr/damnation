#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The container entity class                                         #
#------------------------------------------------------------------------------#

__all__ = ["Container"]

from Classes.hooks import entity_mixin
from carriable import Carriable
from Classes.Gui.widget import Text


@entity_mixin
class Container(object):
    def __entity_init__(self, node):
        self.contents = self.createProperty('contents', [])
        self.max_bulk = self.createProperty('max_bulk', 10.0) # in liters (cubic decimeters)
        self.max_weight = self.createProperty('max_weight', 10.0) # in kilograms
        self.max_size = self.createProperty('max_size', 10.0) # in decimeters

    def _entity_getWidget(self, widgetName):
        widget = None
        if widgetName == "Contents":
            widget = Text("Contents", self)
            widget.setLabel("Contents:")
            widget.setValue(", ".join([str(x) for x in self.contents.get()]))
            self.widgets[widget] = 'contents'
        return widget

    def _entity_updateWidgets(self, task=None):
        for widget, var in self.widgets.iteritems():
            if var == 'contents':
                widget.setValue(", ".join([str(x) for x in self.contents.get()]))

    def addCarriable(self, item):
        if item in self.contents.get():
            print "Warning: Item already in Container!"
            return False

        if not isinstance(item, Carriable):
            print "Warning: Tried to add a non-Carriable to a Container! Ignoring."
            return False

        if item.size.get() > self.max_size.get():
            print "Too big! FAIL."
            return False

        if sum([i.bulk.get() for i in self.contents.get() + [item]]) > self.max_bulk.get():
            print "Too much bulk! FAIL."
            return False

        if sum([i.getWeight() for i in self.contents.get() + [item]]) > self.max_weight.get():
            print "Too much weight! FAIL."
            return False

        self.contents.set(self.contents.get() + [item])

        self.call_mixin_method('_entity_addCarriable', item)
        return True

    def removeCarriable(self, item):
        contents = self.contents.get()

        if item not in contents:
            print "Warning: Item not in Container."
            return False

        contents.remove(item)
        self.contents.set(contents)

        self.call_mixin_method('_entity_removeCarriable', item)
        return True

    def topCarriable(self):
        contents = self.contents.get()

        if len(contents) > 0:
            return contents[0]
        else:
            return None

    def bottomCarriable(self):
        contents = self.contents.get()

        if len(contents) > 0:
            return contents[-1]
        else:
            return None

    def rotateUp(self):
        contents = self.contents.get()

        if len(contents) > 1:
            contents = contents[1:] + [contents[0]]

        self.call_mixin_method('_entity_rotateUp')

    def rotateDown(self):
        contents = self.contents.get()
        if len(contents) > 1:
            contents = [contents[-1]] + [contents[:-1]]

        self.call_mixin_method('_entity_rotateDown')

    def _entity_setState(self, state, timestamp=None):
        """Set this entity's state.

        state should be a dict, where keys correspond to individual state
            elements.

        """
        self.contents.setFromState(state, timestamp)

    def _entity_getState(self):
        """Get this entity's state.

        Returns a dict, where keys correspond to individual state elements.

        """
        self.contents.addToState(state)

        return state

