#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The usable entity class                                            #
#------------------------------------------------------------------------------#

__all__ = ["Usable"]

from Classes.hooks import entity_mixin
from physical import Physical


@entity_mixin
class Usable(object):
    mixin_dependencies = [Physical]

    def __entity_init__(self, node):
        self.__actions = dict() # {'actionName': bool(mustBeInHand), ...}

    def performAction(self, actionName, isInHand=False):
        if actionName not in self.__actions:
            print "Unknown action name \"%s\" on entity \"%s\"!" % (actionName, str(self))
            return False

        if self.__actions[actionName]['mustBeInHand'] and not isInHand:
            print "Action \"%s\" can't be performed on \"%s\" unless it's in your hand!" % (actionName, str(self))
            return False

        self.__actions[actionName]['callback']()

    def hasAction(self, actionName):
        return actionName in self.__actions

    def addAction(self, actionName, callback, mustBeInHand=False):
        self.__actions[actionName] = {'callback': callback, 'mustBeInHand': mustBeInHand}

