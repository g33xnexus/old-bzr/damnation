#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The moddable entity class                                          #
#------------------------------------------------------------------------------#

__all__ = ["Moddable"]

from Classes.hooks import entity_mixin
from Classes.entity_property import Property
from Classes.Gui.widget import Text, Progress


@entity_mixin
class Moddable(object):
    def __entity_init__(self, node, numModSlots = 5):
        # Force this to use parent class's createProperty() so we don't recurse infinitely on get
        self.modSlots = self.createProperty('modSlots', [None] * numModSlots)

        # Now switch to our custom createProperty().
        self.__moddable_base_createProperty = self.createProperty
        self.createProperty = self.__moddable_createProperty

    def getModSlots(self):
        return self.modSlots.get()

    def attachToModSlot(self, mod, idx):
        modSlots = self.modSlots.get()
        if idx > len(modSlots):
            raise "No slot number %d!" % (idx, )
        if modSlots[idx] is not None:
            raise "Slot %d already occupied!" % (idx, )
        modSlots[idx] = mod
        self.modSlots.set(modSlots)

    def _entity_getWidget(self, widgetName):
        widget = None
        if widgetName == "ModSlots":
            widget = Text("ModSlots", self)
            widget.setLabel("Mod Slots:")
            widget.setValue(str(self.modSlots.get()))
            self.widgets[widget] = 'modSlots'
        return widget

    def _entity_updateWidgets(self, task=None):
        for widget, var in self.widgets.iteritems():
            if var == 'modSlots':
                widget.setValue(str(self.modSlots.get()))

    def getFreeModSlots(self):
        return self.modSlots.get().count(None)

    def getTotalModSlots(self):
        return len(self.modSlots.get())

    def __moddable_createProperty(self, tag, initial=None):
        return ModdableProperty(tag, self, initial)

    def doDamage(self, damage):
        remainder = damage

        for damageType in remainder:
            #TODO: Change protector code so it lumps the damage together for
            # each protector; if a mod protects on multiple damage types, the
            # damage for those types should all be dealt at the same time.
            protectors = []
            for mod in self.modSlots.get():
                if mod is not None and mod.protector[damageType]:
                    protectors.append(mod)

            #print protectors
            while len(protectors) > 0:
                nextProtectors = []
                damageToDeal = remainder[damageType] / len(protectors)
                remainder[damageType] = 0
                for mod in protectors:
                    leftover = mod.doDamage({damageType: damageToDeal})
                    if leftover[damageType] == 0:
                        nextProtectors.append(mod)
                    else:
                        remainder[damageType] += leftover[damageType]
                if remainder[damageType] > 0:
                    protectors = nextProtectors
                else:
                    protectors = []
                #print damageType, remainder, protectors

        if sum(remainder.values()) > 0:
            damagePerSlot = dict()
            for key, value in remainder.iteritems():
                if value > 0:
                    damagePerSlot[key] = value * 0.5 / len(self.modSlots.get())
            for mod in self.modSlots.get():
                if mod is not None:
                    for key in damagePerSlot:
                        if key not in remainder:
                            remainder[key] = 0
                        else:
                            remainder[key] -= damagePerSlot[key]
                    leftover = mod.doDamage(damagePerSlot)
                    for key in leftover:
                        if key not in remainder:
                            remainder[key] = leftover[key]
                        else:
                            remainder[key] += leftover[key]
            return super(Moddable, self).doDamage(remainder)

        else:
            return {'kinetic': 0, 'thermal': 0, 'em': 0}

    def _entity_setState(self, state, timestamp=None):
        """Set this entity's state.

        state should be a dict, where keys correspond to individual state
            elements.

        """
        self.modSlots.setFromState(state, timestamp)

    def _entity_getState(self):
        """Get this entity's state.

        Returns a dict, where keys correspond to individual state elements.

        """
        self.modSlots.addToState(state)
        return state


class ModdableProperty(Property):
    def __init__(self, tag, entity, initial=None):
        Property.__init__(self, tag, entity, initial)

    def __get__(self, obj, type=None):
        value = self.value
        for mod in self.entity.getModSlots():
            if mod.modifiesProperty(self.tag):
                value *= mod.getPropertyModifier(self.tag)
        return value

