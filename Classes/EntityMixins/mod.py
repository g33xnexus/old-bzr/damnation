#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The base mod entity class                                          #
#------------------------------------------------------------------------------#

__all__ = ["Mod"]

from Classes.hooks import entity_mixin


@entity_mixin
class Mod(object):
    def __entity_init__(self, node):
        self.size = self.createProperty('size', 1)
        self.propertyMultipliers = self.createProperty('propertyMultipliers', {})
        self.serviceMultipliers = self.createProperty('serviceMultipliers', {})
        self.protector = self.createProperty('protector', {'kinetic': False, 'thermal': False, 'em': False})

    def getSlotSize(self):
        return self.size

    def canAttach(self, moddable):
        return moddable.getFreeSlots() >= self.size


    def modifiesProperty(self, tag):
        return tag in self.propertyMultipliers.get()

    def getPropertyMultiplier(self, tag):
        return self.propertyMultipliers.get().get(tag)

    def getPropertyMultipliers(self):
        return self.propertyMultipliers.get()


    def modifiesService(self, service):
        return service in self.serviceMultipliers.get()

    def getServiceMultiplier(self, service):
        return self.serviceMultipliers.get().get(service)

    def getServiceMultipliers(self):
        return self.serviceMultipliers.get()


    def _entity_setState(self, state, timestamp=None):
        """Set this entity's state.

        state should be a dict, where keys correspond to individual state
            elements.

        """
        self.size.setFromState(state, timestamp)
        self.propertyMultipliers.setFromState(state, timestamp)
        self.serviceMultipliers.setFromState(state, timestamp)
        self.protector.setFromState(state, timestamp)

    def _entity_getState(self):
        """Get this entity's state.

        Returns a dict, where keys correspond to individual state elements.

        """
        self.size.addToState(state)
        self.propertyMultipliers.addToState(state)
        self.serviceMultipliers.addToState(state)
        self.protector.addToState(state)

        return state

