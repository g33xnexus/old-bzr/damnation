#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The base physical object entity class. Any entity which needs      #
#           physical interactions with other objects should inherit from this. #
#------------------------------------------------------------------------------#

__all__ = ["Physical"]

from direct.task import Task
from pandac.PandaModules import VBase3, NodePath, Point3, Vec3, Quat
from panda3d.physx import PhysxBodyDesc, PhysxActorDesc, PhysxEnums

from Classes.util import ColliderUtil
from Classes.hooks import entity_mixin
from Classes.Gui.widget import Text, Progress


@entity_mixin
class Physical(object):
    lastID = 0
    collisionsThisFrame = set()
    elasticity = 0.5

    debugRemovedEntities = False

    def __entity_init__(self, node):
        """Create a behavior for the given node."""

        # Properties
        self.mass = self.createProperty('mass', 1) # in kilograms
        self.mass.onChange.append(self._setMass)
        self.linearVelocity = self.createProperty('linearVelocity', [0, 0, 0]) # in decimeters per second
        self.linearVelocity.onChange.append(self._setLinearVelocity)
        self.angularVelocity = self.createProperty('angularVelocity', [0, 0, 0]) # TODO: is this in degrees per second? radians per second?
        self.angularVelocity.onChange.append(self._setAngularVelocity)

        self.actor = None
        self.newShapes = list()

    def _entity_finishedLoading(self):
        bodyDesc = PhysxBodyDesc()
        bodyDesc.setMass(self.mass.get())

        if self.actor is not None:
            print "ERROR: Tried creating an actor for %s, but one already exists!!!" % (str(self), )
            return

        print "Creating actor for", str(self)
        actorDesc = PhysxActorDesc()
        actorDesc.setName(str(self))
        actorDesc.setBody(bodyDesc)

        for shape in self.newShapes:
            actorDesc.addShape(shape)
        self.newShapes = list()

        self.actor = self.contentLoader.getWorld().scene.createActor(actorDesc)
        self.actor.setGroup(1)
        self.actor.setShapeGroup(1)
        self.actor.attachNodePath(self.node)

        self.actor.setGlobalPos(self.node.getPos(render))
        hpr = self.node.getHpr(render)
        self.actor.setGlobalHpr(hpr[0], hpr[1], hpr[2])

        self.actor.setPythonTag('behavior', self)
        print "  Actor created:", self.actor.getName()

    def loadCollider(self, filename):
        print "Loading collider from \"%s\" for %s." % (filename, str(self))
        shapeDesc = ColliderUtil.loadCollider(filename)

        if shapeDesc is not None:
            if self.actor is not None:
                self.actor.createShape(shapeDesc)
                self.actor.updateMassFromShapes(0, self.mass.get())
            else:
                self.newShapes.append(shapeDesc)
        else:
            print "  Couldn't load collider from \"%s\"!" % (filename, )

    def createCollider(self, definition):
        print "Creating collider \"%s\" for %s." % (definition, str(self))
        shapeDesc = ColliderUtil.createCollider(definition)

        if shapeDesc is not None:
            if self.actor is not None:
                self.actor.createShape(shapeDesc)
                self.actor.updateMassFromShapes(0, self.mass.get())
            else:
                self.newShapes.append(shapeDesc)
        else:
            print "  Error processing collider definition \"%s\"!" % (definition, )

    def _setMass(self, prop):
        self.actor.updateMassFromShapes(0, self.mass.get())

    def _entity_setPosition(self, prop):
        if self.actor is not None:
            self.actor.setGlobalPos(self.node.getPos(render))

    def _entity_setHpr(self, prop):
        if self.actor is not None:
            hpr = self.node.getHpr(render)
            self.actor.setGlobalHpr(hpr.getX(), hpr.getY(), hpr.getZ())

    def _setLinearVelocity(self, prop):
        if self.actor is not None:
            self.actor.setLinearVelocity(Vec3(*self.linearVelocity.get()))

    def _setAngularVelocity(self, prop):
        if self.actor is not None:
            self.actor.setAngularVelocity(Vec3(*self.angularVelocity.get()))

    def _entity_hide(self):
        if self.actor is not None:
            self.actor.putToSleep()
            self.actor.setActorFlag(PhysxEnums.AFDisableCollision, True)

    def _entity_show(self):
        if self.actor is not None:
            self.actor.wakeUp()
            self.actor.setActorFlag(PhysxEnums.AFDisableCollision, False)

    def getWeight(self):
        return self.mass.get() * self.contentLoader.getWorld().getGravity().length()

    def collisionStart(self, other, contacts):
        self.call_mixin_method('_entity_collisionStart', other, contacts)

    def collisionEnd(self, other, contacts):
        self.call_mixin_method('_entity_collisionEnd', other, contacts)

    def collisionContinue(self, other, contacts):
        self.call_mixin_method('_entity_collisionContinue', other, contacts)

    #TODO: Move this elsewhere, or get rid of it!
    @staticmethod
    def findActorNodePath(nodePath):
        while not nodePath.isEmpty() and not isinstance(nodePath.node(), ActorNode):
            nodePath = nodePath.getParent()
        return nodePath

    def addImpulse(self, vec):
        self.actor.addForce(Vec3(vec[0], vec[1], vec[2]), PhysxEnums.FMImpulse)

    def addLocalImpulse(self, vec):
        self.actor.addLocalForce(Vec3(vec[0], vec[1], vec[2]), PhysxEnums.FMImpulse)

    def doMovement(self, stepSize):
        if self.actor is None:
            if Physical.debugRemovedEntities:
                print "Tried to do movement on a Physical with no actor!"
                return
        self.call_mixin_method('_entity_doMovement', stepSize)

    def updatePhysics(self):
        if self.node is None or self.actor is None:
            if Physical.debugRemovedEntities:
                print "Tried to do physics on an invalid entity!"
            return

        # Update the position and orientation of the node.
        pos = self.actor.getGlobalPos()
        self.position.set([pos.getX(), pos.getY(), pos.getZ()])

        # Use the orientation quaternion to translate euler angles into object space.
        angVelWorld = self.actor.getAngularVelocity()
        angVelQuatWorld = Quat(0, angVelWorld)
        orientationQuat = Quat(self.actor.getGlobalQuat())
        angVelQuatLocal = orientationQuat * angVelQuatWorld * orientationQuat.conjugate()
        self.angularVelocity.set([angVelQuatLocal.getI(), angVelQuatLocal.getJ(), angVelQuatLocal.getK()])

        # Retrieve the current linear velocity.
        vel = self.actor.getLinearVelocity()
        self.linearVelocity.value = [vel.getX(), vel.getY(), vel.getZ()]

    def _entity_setState(self, state, timestamp=None):
        """Set this entity's state.

        state should be a dict, where keys correspond to individual state
            elements.

        """
        if self.actor is not None:
            self.mass.setFromState(state, timestamp)
            self.linearVelocity.setFromState(state, timestamp)
            self.angularVelocity.setFromState(state, timestamp)

            if self.actor.isSleeping():
                print "Waking up", self.actor
                self.actor.wakeUp()

    def _entity_getState(self):
        """Get this entity's state.

        Returns a dict, where keys correspond to individual state elements.

        """
        self.mass.addToState(state)
        self.linearVelocity.addToState(state)
        self.angularVelocity.addToState(state)

        return state

    def _entity_getWidget(self, widgetName):
        widget = None
        if widgetName == "SlideVelocity":
            widget = Text("SlideVelocity", self)
            widget.setLabel("Slide:")
            widget.setValue(str(self.linearVelocity.get()[0]))
            self.widgets[widget] = 'slideVelocity'
        elif widgetName == "ForwardVelocity":
            widget = Text("ForwardVelocity", self)
            widget.setLabel("Forward:")
            widget.setValue(str(self.linearVelocity.get()[1]))
            self.widgets[widget] = 'forwardVelocity'
        elif widgetName == "LiftVelocity":
            widget = Text("LiftVelocity", self)
            widget.setLabel("Lift:")
            widget.setValue(str(self.linearVelocity.get()[2]))
            self.widgets[widget] = 'liftVelocity'
        elif widgetName == "Velocity":
            widget = Text("Velocity", self)
            widget.setLabel("Velocity:")
            text = map(self.getHumanReadable, ('forwardVelocity', 'slideVelocity', 'liftVelocity'))
            text = ", ".join([x for x in text if x != "stationary"])
            widget.setValue(text)
            self.widgets[widget] = 'velocity'
        elif widgetName == "Position":
            widget = Text("Position", self)
            widget.setLabel("Position:")
            pos = self.position.get()
            text = "X: %.2f m, Y: %.2f m, Z: %.2f m" % (pos[0], pos[1], pos[2])
            widget.setValue(text)
            self.widgets[widget] = 'position'
        elif widgetName == "Rotation":
            widget = Text("Rotation", self)
            widget.setLabel("Rotation:")

            vel = self.angularVelocity.get()
            text = u"Yaw: %.2f \xc2/s, Pitch: %.2f \xc2/s, Roll: %.2f \xc2/s" % (vel[0], vel[1], vel[2])
            widget.setValue(text)
            self.widgets[widget] = 'rotation'
        return widget

    def _entity_updateWidgets(self, task=None):
        for widget, var in self.widgets.iteritems():
            if var == 'slideVelocity':
                text = "%.2f m/s" % (self.linearVelocity.get()[0], )
                widget.setValue(text)
            elif var == 'forwardVelocity':
                text = "%.2f m/s" % (self.linearVelocity.get()[1], )
                widget.setValue(text)
            elif var == 'liftVelocity':
                text = "%.2f m/s" % (self.linearVelocity.get()[2], )
                widget.setValue(text)
            elif var == 'velocity':
                #text = "Forward: %.2f m/s, Slide: %.2f m/s, Lift: %.2f m/s" % (vel[1], vel[0], vel[2])
                text = map(self.getHumanReadable, ('forwardVelocity', 'slideVelocity', 'liftVelocity'))
                text = ", ".join([x for x in text if x != "stationary"])
                if text == "":
                    text = "stationary"
                widget.setValue(text)

            elif var == "position":
                pos = self.position.get()
                text = "X: %.2f m, Y: %.2f m, Z: %.2f m" % (pos[0], pos[1], pos[2])
                widget.setValue(text)

            elif var == "rotation":
                vel = self.angularVelocity.get()
                text = u"Yaw: %.2f \xc2/s, Pitch: %.2f \xc2/s, Roll: %.2f \xc2/s" % (vel[0], vel[1], vel[2])
                widget.setValue(text)

    def getHumanReadable(self, field):
        if field == 'forwardVelocity':
            currentLinearVelocityLocal = self.node.getRelativeVector(render,
                    VBase3(*self.linearVelocity.get()))
            vel = currentLinearVelocityLocal[1]
            if int(vel * 100) > 0:
                return "%.2f m/s forward" % (vel, )
            elif int(vel * 100) < 0:
                return "%.2f m/s backward" % (-vel, )
            else:
                return "stationary"

        elif field == 'slideVelocity':
            currentLinearVelocityLocal = self.node.getRelativeVector(render,
                    VBase3(*self.linearVelocity.get()))
            vel = currentLinearVelocityLocal[0]
            if int(vel * 100) > 0:
                return "%.2f m/s right" % (vel, )
            elif int(vel * 100) < 0:
                return "%.2f m/s left" % (-vel, )
            else:
                return "stationary"

        elif field == 'liftVelocity':
            currentLinearVelocityLocal = self.node.getRelativeVector(render,
                    VBase3(*self.linearVelocity.get()))
            vel = currentLinearVelocityLocal[2]
            if int(vel * 100) > 0:
                return "%.2f m/s up" % (vel, )
            elif int(vel * 100) < 0:
                return "%.2f m/s down" % (-vel, )
            else:
                return "stationary"

    def _entity_remove(self):
        self.actor.putToSleep()
        self.actor.setActorFlag(PhysxEnums.AFDisableCollision, True)
        self.actor.detachNodePath()

        #self.actor.release()
        self.contentLoader.getWorld().removeActor(self.actor)

        self.actor = None

