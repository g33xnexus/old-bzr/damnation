#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#                                                                              #
#    Brief: The damageable entity class                                        #
#------------------------------------------------------------------------------#

__all__ = ["Damageable"]

from direct.task import Task

from Classes.hooks import entity_mixin
from Classes.Gui.widget import Text, Progress


@entity_mixin
class Damageable(object):
    def __entity_init__(self, node):
        self.maxStrength = self.createProperty('max_strength', 2695)
        self.strength = self.createProperty('strength', self.maxStrength.get())
        self.resistance = self.createProperty('resistance', {'kinetic': 0.001, 'thermal': 0.001, 'em': 0.001})
        self.regenRate = self.createProperty('regen_rate', 0)

        # Damageable regenStrength task
        self.regenTask = taskMgr.doMethodLater(1, self.regenStrength, "Regenerate damageable strength over time.")

    def _entity_getWidget(self, widgetName):
        widget = None
        if widgetName == "Damage":
            widget = Progress("Damage", self)
            widget.setLabel("Damage:")
            widget.setValue(self.strength.get())
            widget.setMaxValue(self.maxStrength.get())
            self.widgets[widget] = 'damage'
        return widget

    def _entity_updateWidgets(self, task=None):
        for widget, var in self.widgets.iteritems():
            if var == 'damage':
                widget.setValue(self.strength.get())

    def doDamage(self, damage):
        kinetic = 0

        dealt = dict()
        for damageType in damage:
            dealt[damageType] = damage[damageType] * (1 - self.resistance[damageType])

        totalDamage = sum(dealt.values())
        self.strength.set(self.strength.get() - totalDamage)

        remainder = {'kinetic': 0, 'thermal': 0, 'em': 0}

        if (self.strength.get() < 0):
            for damageType in dealt:
                remainder[damageType] = abs(self.strength.get()) * (dealt[damageType] / totalDamage)

            self.strength.set(0)

        self.call_mixin_method('_entity_doDamage', damage)

        if self.strength.get() == 0:
            self.call_mixin_method('_entity_strengthAtZero')

        return remainder

    def regenStrength(self, task):
        if self.strength.get() < self.maxStrength.get():
            gained = self.maxStrength.get() * self.regenRate.get()
            self.strength.set(self.strength.get() + gained)
        #print "Debug: Reset strength to strength + gained:", self.strength.get()

        if (self.strength.get() > self.maxStrength.get()):
            self.strength.set(self.maxStrength.get())
            #print "Debug: Strength overflow: Reset strength to max_strength:", self.strength.get()

        return Task.again

    def _entity_setState(self, state, timestamp=None):
        """Set this entity's state.

        state should be a dict, where keys correspond to individual state
            elements.

        """
        self.strength.setFromState(state, timestamp)
        #print "Debug: State set strength to:", self.strength.get()
        self.maxStrength.setFromState(state, timestamp)
        #print "Debug: State set max_strength to:", self.strength.get()
        self.regenRate.setFromState(state, timestamp)
        self.resistance.setFromState(state, timestamp)

    def _entity_getState(self):
        """Get this entity's state.

        Returns a dict, where keys correspond to individual state elements.

        """
        self.strength.addToState(state)
        self.maxStrength.addToState(state)
        self.regenRate.addToState(state)
        self.resistance.addToState(state)

        return state

    def remove(self):
        taskMgr.remove(self.regenTask)

