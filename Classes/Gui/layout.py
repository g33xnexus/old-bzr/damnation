#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The GUI layout class                                               #
#------------------------------------------------------------------------------#

import os
import xml.sax

import Classes.overlays as overlays
import widget


class LayoutManager(object):
    """This class manages the current layout, including all currently visible
    widgets.

    """
    def __init__(self):
        self.base = None
        self.widgets = []

    def loadLayout(self, layoutFilename):
        self.clearWidgets()

        self.base = overlays.PixelNode('layoutBasePixel2D')
        base.accept('aspectRatioChanged', self.base.aspectRatioChanged)

        #layout = file(layoutFilename)
        xml.sax.parse(os.path.join("Gui", "Layouts", layoutFilename), LayoutHandler())

    def addWidget(self, widgetObject):
        self.widgets.append(widgetObject)

    def clearWidgets(self):
        for widgetObject in self.widgets:
            widgetObject.remove()
        self.base = None

    def getTheme(self):
        return self.theme

    def setTheme(self, theme):
        self.theme = theme

        for widgetObject in self.widgets:
            widgetObject.themeLoaded()


manager = LayoutManager()


class LayoutHandler(xml.sax.handler.ContentHandler):
    def __init__(self):
        self.currentElements = list()
        self.rootElement = None

    def startElement(self, name, attrs):
        global manager

        if self.rootElement is None:
            if name == "layout":
                self.rootElement = name
            else:
                raise Exception("Invalid root element in layout: '%s' (should be 'layout')" % (name, ))

        else:
            if len(self.currentElements) > 0 and "addChild" not in dir(self.currentElements[-1]):
                raise Exception("Tried to nest '%s' inside '%s', which has no 'addChild' method!"
                        % (str(name), str(self.currentElements.__class__.__name__)))

            try:
                (provider, widgetName) = name.split(".")
                newWidget = widget.getWidget(provider, widgetName)

                if newWidget is None:
                    raise Exception("Couldn't find widget \"%s\" from provider \"%s\"!" % (widgetName, provider))

            except Exception, e:
                import traceback
                traceback.print_exc()
                raise Exception("Invalid widget in layout: " + name)

            for key, value in attrs.items():
                newWidget.setAttribute(key, value)

            newWidget.update()

            if len(self.currentElements) > 0:
                self.currentElements[-1].addChild(newWidget)

            self.currentElements.append(newWidget)
            manager.addWidget(newWidget)

    def endElement(self, name):
        if len(self.currentElements) == 0:
            if name == 'layout':
                self.rootElement = None
            else:
                raise Exception("Invalid end element '%s'!" % (name, ))
        else:
            self.currentElements.pop()

