#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The GUI widget element classes                                     #
#------------------------------------------------------------------------------#

from pandac.PandaModules import Vec4, TextNode, Texture

import Classes.overlays as overlays
import widget
import layout


overlays.TextOverlay.defaultFont = overlays.TextOverlay.loadFont("Fonts/antigravbb_reg.otf", size=12)


class WidgetElement(object):
    def __init__(self, name, widget):
        self.name = name
        self.widget = widget
        self.settings = dict()

    def themeLoaded(self):
        """Called when the element is created, and whenever a theme is loaded.
        
        Each element type should load its settings here.

        """
        pass

    def getSettings(self, **keys):
        providerName = ""
        for provName, provider in widget.widgetProviders.iteritems():
            if provider == self.widget.getProvider():
                providerName = provName

        return layout.manager.getTheme().getValues(
                providerName,
                self.widget.getName(),
                self.widget.getTags(),
                self.name,
                keys)

    def toColor(self, colorString):
        if colorString[0] == "#":
            colorString = colorString[1:]
            if len(colorString) == 3:
                return Vec4(int(colorString[0], 16) / 256.0, int(colorString[1], 16) / 256.0, int(colorString[2], 16) / 256.0, 1)
            elif len(colorString) == 4:
                return Vec4(int(colorString[0], 16) / 256.0, int(colorString[1], 16), int(colorString[2], 16), int(colorString[3], 16))
            elif len(colorString) == 6:
                return Vec4(int(colorString[0:2], 16) / 256.0, int(colorString[2:4], 16) / 256.0, int(colorString[4:6], 16) / 256.0, 1)
            elif len(colorString) == 8:
                return Vec4(int(colorString[0:2], 16) / 256.0, int(colorString[2:4], 16) / 256.0, int(colorString[4:6], 16) / 256.0, int(colorString[6:8], 16) / 256.0)
            raise Exception("Incorrect number of characters in color '%s'!" % (colorString, ))

        elif (colorString.startswith("rgb(") or colorString.startswith("rgba(")) and colorString.endswith(")"):
            elements = [x.strip() for x in ",".split(colorString[colorString.index("(") + 1:colorString.index(")")])]
            if len(elements) == 3:
                return Vec4(int(elements[0], 10) / 256.0, int(elements[1], 10) / 256.0, int(elements[2], 10) / 256.0, 1)
            elif len(elements) == 4:
                return Vec4(int(elements[0], 10) / 256.0, int(elements[1], 10) / 256.0, int(elements[2], 10) / 256.0, int(elements[3], 10) / 256.0)
            raise Exception("Incorrect number of elements in color '%s'!" % (colorString, ))

        else:
            raise Exception("Unknown color '%s'!" % (colorString, ))

    def toEnum(self, value, choices):
        if value.lower() in choices:
            return choices[value.lower()]
        raise Exception("Unknown enum value '%s'!" % (choices, ))

    def reparentTo(self, base):
        pass


class Box(WidgetElement):
    def __init__(self, name, widget):
        WidgetElement.__init__(self, name, widget)

        self.overlay = overlays.OverlayContainer()
        self.backgroundContainer = overlays.OverlayContainer()
        self.backgroundContainer.reparentTo(self.overlay)
        self.background = None
        self.width = 0
        self.height = 0
        self.padding = 0

    def themeLoaded(self):
        self.settings = self.getSettings(
                color="#00000000",
                width=None,
                height=None,
                background=None,
                padding=0)

        self.padding = self.settings["padding"]

        if self.settings["width"] is not None:
            self.width = self.settings["width"]
        if self.settings["height"] is not None:
            self.height = self.settings["height"]

        if self.settings["background"] is not None:
            if self.background is not None:
                self.background.reparentTo(None)

            background = self.settings["background"]

            paddingLeft, paddingRight, paddingTop, paddingBottom = 0, 0, 0, 0
            if background.count(" ") == 0:
                # "image"
                image = background
            elif background.count(" ") == 1:
                # "image padding"
                (image, padding) = background.split(" ")
                (paddingTop, paddingRight, paddingBottom, paddingLeft) = [padding] * 4
            elif background.count(" ") == 2:
                # "image padding-left padding-right"
                (image, paddingLeft, paddingRight) = background.split(" ")
            elif background.count(" ") == 4:
                # "image padding-top padding-right padding-bottom padding-left"
                (image, paddingTop, paddingRight, paddingBottom, paddingLeft) = background.split(" ")

            paddingLeft, paddingRight, paddingTop, paddingBottom = \
                    int(paddingLeft), int(paddingRight), int(paddingTop), int(paddingBottom)

            texture = layout.manager.getTheme().getImage(image)
            texture.setMagfilter(Texture.FTNearest)
            texture.setMagfilter(Texture.FTLinearMipmapLinear)

            if paddingTop == 0 and paddingBottom == 0:
                if paddingLeft == 0 and paddingRight == 0:
                    self.background = overlays.Overlay(texture=texture,
                            color=Vec4(1, 1, 1, 1))
                else:
                    self.background = overlays.OverlaySlice3(texture=texture,
                            color1=Vec4(1, 1, 1, 1),
                            edges=(paddingLeft, paddingRight))
            else:
                self.background = overlays.OverlaySlice9(texture=texture, color1=Vec4(1, 1, 1, 1),
                        edges=(paddingTop, paddingLeft, paddingBottom, paddingRight),
                        texcoords=(0, 0, texture.getXSize(), texture.getYSize()))

            self.background.setPos(0, 0)
            self.background.setSize(self.width, self.height)
            self.background.reparentTo(self.backgroundContainer)

        elif self.settings["color"] is not None:
            if self.background is not None:
                self.background.reparentTo(None)

            self.background = overlays.Overlay(color=self.toColor(self.settings["color"]))

            self.background.setPos(0, 0)
            self.background.setSize(self.width, self.height)
            self.background.reparentTo(self.backgroundContainer)

        self.widget.update()

    def setPos(self, x, y):
        self.overlay.setPos(x, y)

    def getPos(self):
        return self.overlay.getPos()

    def setSize(self, x, y):
        if x is not None:
            self.width = x
        if y is not None:
            self.height = y
        if self.background is not None:
            self.background.setSize(self.width, self.height)

    def getSize(self):
        size = list(self.overlay.getSize())
        try:
            if self.settings["width"] is not None:
                size[0] = int(self.settings["width"])
            if self.settings["height"] is not None:
                size[1] = int(self.settings["height"])
        except KeyError:
            pass
        return size

    def reparentTo(self, base):
        self.overlay.reparentTo(base)

    def getOverlay(self):
        return self.overlay

class Text(WidgetElement):
    fontCache = dict()

    def __init__(self, name, widget):
        WidgetElement.__init__(self, name, widget)

        self.textOverlay = overlays.TextOverlay(name=name,
                            color=Vec4(1, 1, 1, 1))

        self.font = None

    def themeLoaded(self):
        self.settings = self.getSettings(
                font="Fonts/smoothansi.pcf",
                fontSize=None,
                color="#FFFFFF",
                align="left",
                width=None,
                height=None,
                wordwrap=None)

        if self.settings["color"] is not None:
            self.textOverlay.textNode.setTextColor(self.toColor(self.settings["color"]))

        if self.settings["align"] is not None:
            self.textOverlay.setAlign(self.toEnum(self.settings["align"], {
                    "left": TextNode.ALeft,
                    "right": TextNode.ARight,
                    "center": TextNode.ACenter,
                    "boxedleft": TextNode.ABoxedLeft,
                    "boxedright": TextNode.ABoxedRight,
                }))

        if self.settings["wordwrap"] is not None:
            self.textOverlay.setWordwrap(self.settings["wordwrap"])

        if self.settings["font"] is not None:
            if (self.settings["font"], self.settings["fontSize"]) in Text.fontCache:
                self.font = Text.fontCache[self.settings["font"], self.settings["fontSize"]]
            else:
                if self.settings["fontSize"] is not None:
                    print "Loading font:", self.settings["font"], self.settings["fontSize"]
                    self.font = overlays.TextOverlay.loadFont(self.settings["font"], size=self.settings["fontSize"])
                else:
                    print "Loading font:", self.settings["font"]
                    self.font = overlays.TextOverlay.loadFont(self.settings["font"])
                Text.fontCache[self.settings["font"], self.settings["fontSize"]] = self.font
            self.textOverlay.setFont(self.font)

        self.widget.update()

    def setPos(self, x, y):
        self.textOverlay.setPos(x, y)

    def getPos(self):
        return self.textOverlay.getPos()

    def getSize(self):
        size = list(self.textOverlay.getSize())
        try:
            if self.settings["width"] is not None:
                size[0] = int(self.settings["width"])
            if self.settings["height"] is not None:
                size[1] = int(self.settings["height"])
        except KeyError:
            pass
        return size

    def setText(self, text):
        self.textOverlay.setText(text)

    def reparentTo(self, base):
        self.textOverlay.reparentTo(base)

