#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The GUI widget classes                                             #
#------------------------------------------------------------------------------#

from pandac.PandaModules import Vec4
from direct.task import Task

from Classes.configuration import Configuration
import Classes.overlays

import elements
import layout


__all__ = []
def export(obj):
    global __all__
    __all__.append(obj.__name__)
    return obj


widgetProviders = {}

@export
def widgetProvider(cls, name=None):
    global widgetProviders
    if "getWidget" not in dir(cls):
        raise Exception("Tried to register provider " + cls.__name__
            + " that doesn't implement getWidget()!")
    if name is None:
        name = cls.__name__
    print 'Registering widget provider "%s".' % (name, )
    widgetProviders[name] = cls
    return cls

@export
def getWidget(provider, widget):
    global widgetProviders
    if provider not in widgetProviders:
        print "Widget Providers:", widgetProviders
        raise Exception("Provider %s not found!" % (provider, ))
    return widgetProviders[provider].getWidget(widget)


class Widget(object):
    def __init__(self, name, provider):
        self.name = name
        self.provider = provider
        self.tags = list()
        self.elements = dict()
        self.base = elements.Box("background", self)
        self.base.reparentTo(layout.manager.base)
        self.elements = {"base": self.base}

        self.x, self.y = 0, 0
        self.width, self.height = None, None

    def setAttribute(self, key, value):
        if key == "name":
            self.name = value
        elif key == "x":
            self.x = int(value)
        elif key == "y":
            self.y = int(value)
        elif key == "width":
            self.width = int(value)
        elif key == "height":
            self.height = int(value)
        elif key == "tags":
            self.tags = [x for x in attrs["tags"].split(" ") if x != ""]
        else:
            raise Exception("Unknown attribute key: " + unicode(key).encode('latin-1'))

    def addElement(self, element):
        self.elements[element.name] = element
        element.reparentTo(self.base.getOverlay())

    def setTags(self, tags):
        self.tags = tags

    def getTags(self):
        return self.tags

    def getName(self):
        return self.name

    def setName(self, name):
        self.name = name

    def getProvider(self):
        return self.provider

    def getElements(self):
        return self.elements.values()

    def themeLoaded(self):
        for element in self.elements.values():
            element.themeLoaded()

    def setPos(self, x, y):
        self.x, self.y = x, y
        self.update()

    def getPos(self):
        return self.x, self.y

    def setSize(self, width, height):
        self.setWidth(width)
        self.setHeight(height)

    def setWidth(self, width):
        if width is None:
            self.width = None
        else:
            self.width = int(width)
        self.update()

    def setHeight(self, height):
        if height is None:
            self.height = None
        else:
            self.height = int(height)
        self.update()

    def getSize(self):
        return self.getWidth(), self.getHeight()

    def getWidth(self):
        if self.width is not None and self.width < 0:
            return base.win.getXSize() + self.width
        else:
            return self.width

    def getHeight(self):
        if self.height is not None and self.height < 0:
            return base.win.getYSize() + self.height
        else:
            return self.height

    def getMinimumSize(self):
        return (self.getMinimumWidth(), self.getMinimumHeight())

    def getMinimumWidth(self):
        if self.width is not None:
            return self.width
        else:
            otherItemsWidths = [widget.getSize()[0] for key, widget in self.elements.iteritems() if key != "base" and widget.getSize()[0] is not None]
            if len(otherItemsWidths) > 0:
                return min(otherItemsWidths)
            else:
                return self.base.width 

    def getMinimumHeight(self):
        if self.height is not None:
            return self.height
        else:
            otherItemsHeights = [widget.getSize()[1] for key, widget in self.elements.iteritems() if key != "base" and widget.getSize()[1] is not None]
            if len(otherItemsHeights) > 0:
                return min(otherItemsHeights)
            else:
                return self.base.height

    def update(self, width=None, height=None):
        if self.width is not None:
            width = self.getWidth()
        if self.height is not None:
            height = self.getHeight()

        self.base.setSize(width, height)

        # Set the widget position
        x, y = self.getPos()

        if x is None:
            x = 0
        elif x < 0:
            x = base.win.getXSize() + x - width

        if y is None:
            y = 0
        elif y < 0:
            y = base.win.getYSize() + y - height

        self.base.setPos(x, y)

    def reparentTo(self, parent):
        self.base.reparentTo(parent)

    def remove(self):
        for element in self.elements.values():
            element.reparentTo(None)


@export
class Text(Widget):
    def __init__(self, name, provider):
        Widget.__init__(self, name, provider)

        self.x, self.y = 0, 0
        self.width, self.height = None, None
        self.label = ""
        self.value = 0
        self.hideLabel = False

        self.addElement(elements.Text("label", self))
        self.addElement(elements.Text("value", self))

        self.themeLoaded()

    def setAttribute(self, key, value):
        if key == "hideLabel":
            self.hideLabel = (value.lower() in ["true", "t", "1", "on", "yes"])
        else:
            Widget.setAttribute(self, key, value)

    def update(self):
        padding = self.base.padding

        nextX = padding

        if self.hideLabel:
            self.label = ""
            self.elements["label"].setText("")

        if self.label != "":
            self.elements["label"].setPos(padding, padding)
            nextX += self.elements["label"].getSize()[0] + padding

        self.elements["value"].setPos(nextX, padding)
        nextX += self.elements["value"].getSize()[0] + padding

        width, height = self.getWidth(), self.getHeight()
        if width is None:
            width = nextX
        if height is None:
            height = max(self.elements["label"].getSize()[1] + (2 * padding), self.elements["value"].getSize()[1] + (2 * padding))

        Widget.update(self, width, height)

    def getMinimumWidth(self):
        minWidth = self.getWidth()
        if minWidth is None:
            padding = self.base.padding
            minWidth = padding

            if self.hideLabel:
                self.label = ""

            if self.label != "":
                minWidth += self.elements["label"].getSize()[0] + padding

            minWidth += self.elements["value"].getSize()[0] + padding
        return minWidth

    def getMinimumHeight(self):
        minHeight = self.getHeight()
        if minHeight is None:
            padding = self.base.padding

            minHeight = max(self.elements["label"].getSize()[1] + (2 * padding), self.elements["value"].getSize()[1] + (2 * padding))
        return minHeight

    def setLabel(self, label):
        self.label = label
        self.elements["label"].setText(unicode(label).encode('latin-1'))
        self.update()

    def setValue(self, value):
        self.value = value
        self.elements["value"].setText(unicode(value).encode('latin-1'))
        self.update()


@export
class Progress(Widget):
    def __init__(self, name, provider):
        Widget.__init__(self, name, provider)

        self.x, self.y = 0, 0
        self.width, self.height = None, None
        self.label = ""
        self.value = 0
        self.maxValue = 100
        self.hideLabel = False

        self.addElement(elements.Text("label", self))
        self.addElement(elements.Box("empty", self))
        self.addElement(elements.Box("full", self))
        self.addElement(elements.Text("value", self))

        self.themeLoaded()

    def setAttribute(self, key, value):
        if key == "hideLabel":
            self.hideLabel = (value.lower() in ["true", "t", "1", "on", "yes"])
        else:
            Widget.setAttribute(self, key, value)

    def update(self):
        #TODO: Hide label if needed, and do correct positioning using the same method as Text.
        padding = self.base.padding

        self.elements["label"].setPos(padding, padding)

        self.elements["empty"].setPos(self.elements["label"].getSize()[0] + (2 * padding), padding)
        self.elements["full"].setPos(self.elements["label"].getSize()[0] + (2 * padding), padding)

        width, height = self.getWidth(), self.getHeight()
        if width is None:
            width = self.elements["label"].getSize()[0] + (13 * padding)
        if height is None:
            height = self.elements["label"].getSize()[1] + (2 * padding)

        barWidth = width - self.elements["label"].getSize()[0] - (3 * padding)
        self.elements["empty"].setSize(barWidth, height - (2 * padding))
        value = self.value
        if value < 0:
            value = -value
            if self.elements["full"].background is not None:
                self.elements["full"].background.setColor(Vec4(1, 0, 0, 1))
        else:
            if self.elements["full"].background is not None:
                self.elements["full"].background.setColor(Vec4(1, 1, 1, 1))

        self.elements["full"].setSize(int(barWidth * (value / float(self.maxValue))), height - (2 * padding))

        valueWidth = self.elements["value"].getSize()[0]
        self.elements["value"].setPos(self.elements["label"].getSize()[0] + (2 * padding) + \
            (barWidth / 2) - (valueWidth / 2), padding)

        Widget.update(self, width, height)

    def getMinimumWidth(self):
        #TODO: Audit
        minWidth = self.getWidth()
        if minWidth is None:
            padding = self.base.padding
            minWidth = padding

            if self.hideLabel:
                self.label = ""

            if self.label != "":
                minWidth += self.elements["label"].getSize()[0] + padding

            # Use 50 as the minimum width of the progress bar.
            minWidth += 50 + padding
        return minWidth

    def getMinimumHeight(self):
        #TODO: Audit
        minHeight = self.getHeight()
        if minHeight is None:
            padding = self.base.padding

            minHeight = max(self.elements["label"].getSize()[1] + (2 * padding), self.elements["value"].getSize()[1] + (2 * padding))
        return minHeight

    def setLabel(self, label):
        self.label = label
        self.elements["label"].setText(unicode(label).encode('latin-1'))
        self.update()

    def setValue(self, value):
        self.value = value
        self.elements["value"].setText("%.1f%%" % (100.0 * float(value) / float(self.maxValue), ))
        self.update()

    def setMaxValue(self, maxValue):
        self.maxValue = maxValue
        self.elements["value"].setText("%.1f%%" % (100.0 * float(self.value) / float(maxValue), ))
        self.update()


@export
class HorizontalLayout(Widget):
    def __init__(self, name, provider):
        Widget.__init__(self, name, provider)

        self.children = list()
        self.verticalAlign = "stretch"
        self.horizontalAlign = "stretch"

        self.themeLoaded()

    def addChild(self, widget):
        self.children.append(widget)
        widget.reparentTo(self.base.overlay)
        self.update()

    def setAttribute(self, key, value):
        if key == "verticalAlign":
            self.verticalAlign = value
        elif key == "horizontalAlign":
            self.horizontalAlign = value
        else:
            Widget.setAttribute(self, key, value)

    def update(self):
        padding = self.base.padding

        widths = dict()
        maxChildHeight = 0
        for child in self.children:
            child.setWidth(None)
            size = list(child.getSize())
            if size[0] is None:
                size[0] = child.getMinimumSize()[0]
            if size[1] is None:
                size[1] = child.getMinimumSize()[1]
            if size > 0:
                widths[child] = size[0]
                maxChildHeight = max(maxChildHeight, size[1])

        totalHorizontalPadding = (len(widths) + 1) * padding
        totalOriginalWidths = sum([width for width in widths.values() if width is not None])

        width, height = self.getWidth(), self.getHeight()

        if width is None:
            width = totalOriginalWidths + totalHorizontalPadding
        if height is None:
            height = maxChildHeight + (2 * padding)

        if self.horizontalAlign == "stretch":
            widthToDivide = width - totalHorizontalPadding
            nextX = padding
        else:
            widthToDivide = totalOriginalWidths
            if self.horizontalAlign == "left":
                nextX = padding
            elif self.horizontalAlign == "right":
                nextX = width - (totalOriginalWidths + totalHorizontalPadding) + padding
            elif self.horizontalAlign == "center":
                nextX = (padding + (width - (totalOriginalWidths + totalHorizontalPadding) + padding)) / 2
            else:
                raise Exception("Unknown horizontal alignment value: " + self.verticalAlign)

        for child in self.children:
            if child in widths:
                top = padding
                childHeight = child.getHeight()
                if childHeight is None:
                    childHeight = child.getMinimumHeight()
                if self.verticalAlign == "center":
                    top += (maxChildHeight - childHeight) / 2
                    childHeight = None
                elif self.verticalAlign == "bottom":
                    top += maxChildHeight - childHeight
                    childHeight = None
                elif self.verticalAlign == "stretch":
                    childHeight = maxChildHeight
                elif self.verticalAlign == "top":
                    childHeight = None
                else:
                    raise Exception("Unknown vertical alignment value: " + self.verticalAlign)

                childWidth = int((widths[child] / float(totalOriginalWidths)) * widthToDivide)
                child.setPos(nextX, top)
                child.setSize(childWidth, childHeight)
                nextX += childWidth + padding

        Widget.update(self, width, height)

@export
class VerticalLayout(Widget):
    def __init__(self, name, provider):
        Widget.__init__(self, name, provider)

        self.children = list()
        self.verticalAlign = "stretch"
        self.horizontalAlign = "stretch"

        self.themeLoaded()

    def addChild(self, widget):
        self.children.append(widget)
        widget.reparentTo(self.base.overlay)
        self.update()

    def setAttribute(self, key, value):
        if key == "verticalAlign":
            self.verticalAlign = value
        elif key == "horizontalAlign":
            self.horizontalAlign = value
        else:
            Widget.setAttribute(self, key, value)

    def update(self):
        padding = self.base.padding

        heights = dict()
        maxChildWidth = 0
        for child in self.children:
            child.setHeight(None)
            size = list(child.getSize())
            if size[0] is None:
                size[0] = child.getMinimumSize()[0]
            if size[1] is None:
                size[1] = child.getMinimumSize()[1]
            if size > 0:
                heights[child] = size[1]
                maxChildWidth = max(maxChildWidth, size[1])

        totalVerticalPadding = (len(heights) + 1) * padding
        totalOriginalHeights = sum([height for height in heights.values() if height is not None])

        height, width = self.getHeight(), self.getWidth()

        if height is None:
            height = totalOriginalHeights + totalVerticalPadding
        if width is None:
            width = maxChildWidth + (2 * padding)

        if self.verticalAlign == "stretch":
            heightToDivide = height - totalVerticalPadding
            nextY = padding
        else:
            heightToDivide = totalOriginalHeights
            if self.verticalAlign == "top":
                nextY = padding
            elif self.verticalAlign == "bottom":
                nextY = height - (totalOriginalHeights + totalVerticalPadding) + padding
            elif self.verticalAlign == "center":
                nextY = (padding + (height - (totalOriginalHeights + totalVerticalPadding) + padding)) / 2
            else:
                raise Exception("Unknown vertical alignment value: " + self.verticalAlign)

        for child in self.children:
            if child in heights:
                left = padding
                childWidth = child.getWidth()
                if childWidth is None:
                    childWidth = child.getMinimumWidth()
                if self.horizontalAlign == "center":
                    left += (maxChildWidth - childWidth) / 2
                    childWidth = None
                elif self.horizontalAlign == "right":
                    left = width - childWidth - padding
                    childWidth = None
                elif self.horizontalAlign == "stretch":
                    childWidth = maxChildWidth
                elif self.horizontalAlign == "left":
                    childWidth = None
                else:
                    raise Exception("Unknown horizontal alignment value: " + self.horizontalAlign)

                childHeight = int((heights[child] / float(totalOriginalHeights)) * heightToDivide)
                child.setPos(left, nextY)
                child.setSize(childWidth, childHeight)
                nextY += childHeight + padding

        Widget.update(self, width, height)

@widgetProvider
class Layout(object):
    """This is a class to contain all the general layout widgets.

    """
    widgets = list()
    widgetUpdateStarted = False

    @classmethod
    def getWidget(cls, widgetName):
        if not cls.widgetUpdateStarted:
            taskMgr.doMethodLater(float(Configuration['gui_update_interval']), cls.updateWidgets, "Update the general HUD layout widgets")
            cls.widgetUpdateStarted = True

        newWidget = None
        if widgetName == "Horizontal":
            newWidget = HorizontalLayout("Horizontal", cls)
            cls.widgets.append(newWidget)
        elif widgetName == "Vertical":
            newWidget = VerticalLayout("Vertical", cls)
            cls.widgets.append(newWidget)
        return newWidget

    @classmethod
    def updateWidgets(cls, task=None):
        for widget in cls.widgets:
            widget.update()

        return Task.again

