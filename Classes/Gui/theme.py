#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The GUI theme class                                                #
#------------------------------------------------------------------------------#

import ConfigParser, os


class Theme(object):
    """This class controls the visual style of widgets.

    When the theme is changed, all loaded widgets should be notified.

    """
    def __init__(self, themeName):
        self.themeName = themeName
        self.config = ConfigParser.ConfigParser()
        if not os.path.exists(os.path.join("Gui", "Themes", themeName, "theme.cfg")):
            print "FARK! No theme config!!!"
        self.config.read(os.path.join("Gui", "Themes", themeName, "theme.cfg"))
        self.sections = self.config.sections()

    def getImage(self, imageName):
        return loader.loadTexture(os.path.join("Gui", "Themes", self.themeName, imageName))

    def getValues(self, provider, widget, tags, element, keys):
        def filterSection(section):
            for selector in section.split(","):
                if selector != "DEFAULTS":
                    selectorElements = selector.split(".")
                    sProvider = selectorElements[0]
                    sWidget = "*"
                    sTags = "*"
                    if len(selectorElements) > 1:
                        sWidget = selectorElements[1]
                    if len(selectorElements) > 2:
                        sTags = ".".join(selectorElements[2:])
                    if sProvider in ["*", provider] and sWidget in ["*", widget]:
                        if sTags == "*":
                            return True
                        tagSets = sTags.split("/")
                        for tagSet in tagSets:
                            try:
                                tsTags = tagSet.split("+")
                                for tag in tsTags:
                                    if tag not in tags:
                                        raise Exception()
                                return True
                            except:
                                pass
            return False

        values = dict()
        matchingSections = filter(filterSection, self.sections) + ["DEFAULTS"]
        for key, value in keys.iteritems():
            for section in matchingSections:
                configKey = element + "." + key
                if self.config.has_option(section, configKey):
                    if self.config.get(section, configKey) == "None":
                        value = None
                    elif type(value) is bool:
                        value = self.config.getboolean(section, configKey)
                    elif type(value) is float or type(value) is int:
                        try:
                            if "." in self.config.get(section, configKey):
                                value = self.config.getfloat(section, configKey)
                            else:
                                value = self.config.getint(section, configKey)
                        except ValueError:
                            value = self.config.get(section, configKey)
                    else:
                        value = self.config.get(section, configKey)
                    break
            values[key] = value
        return values

