#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The base entity class. All behaviors should inherit from this.     #
#------------------------------------------------------------------------------#
__all__ = ["Property"]


class Property(object):
    def __init__(self, tag, entity, initial=None):
        self.tag = tag
        self.value = initial
        self.entity = entity
        self.onChange = []

        if entity is None:
            raise Exception("WARNING: No entity passed to property %s!" % (self.entity, ))
        if entity.node is None:
            raise Exception("WARNING: No node in entity %s when initializing property %s!" % (self.entity, self))

        if entity.node.hasTag(tag):
            self.value = entity.node.getTagAsPythonTag(tag)
        else:
            entity.node.setTagAsPythonTag(self.tag, self.value)

    def __str__(self):
        return self.tag

    def __get__(self, obj, type=None):
        return Property.get(self)

    def get(self):
        return self.value

    def __set__(self, obj, value):
        Property.set(self, value)
        return None

    def set(self, value):
        self.value = value
        if self.entity.node is not None:
            self.entity.node.setTagAsPythonTag(self.tag, value)
        else:
            # FIXME: Handle this correctly
            print "WARNING: No node on entity %s when setting property %s!" % (self.entity, self) 
            #raise Exception("WARNING: No node on entity %s when setting property %s!" % (self.entity, self))

        for callback in self.onChange:
            callback(self)

    def __getitem__(self, key):
        return self.value[key]

    def __setitem__(self, key, value):
        self.value[key] = value

    def addToState(self, state):
        state[self.tag] = self.value

    def getValueFromState(self, state, timestamp):
        if self.tag in state:
            return state[self.tag]
        else:
            return self.value

    def setFromState(self, state, timestamp):
        value = self.getValueFromState(state, timestamp)
        if value is not None:
            Property.set(self, value)

