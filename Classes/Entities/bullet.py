#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The bullet entity class                                            #
#------------------------------------------------------------------------------#

__all__ = ["Bullet"]

from Classes.hooks import entity
from base import Base
from Classes.EntityMixins import Physical, Projectile

@entity
class Bullet(Base, Physical, Projectile):
    def __post_entity_init__(self, node):
        pass

    def __str__(self):
        return self.__class__.__name__

    def _post_entity_collisionStart(self, other, contacts):
        otherBehavior = self.contentLoader.getWorld().getBehavior(other)

        if otherBehavior is not None:
            print otherBehavior, otherBehavior.hasMixin("Damageable"), 'doDamage' in dir(other)
            if otherBehavior.hasMixin("Damageable"):
                #TODO: Do something better here? Probably move into Damageable.
                otherBehavior.doDamage({'kinetic': 100})

        otherBehavior = self.contentLoader.getWorld().getBehavior(other)
        print "%s: Collided with %s; removing." % (str(self), str(otherBehavior))
        self.remove()

