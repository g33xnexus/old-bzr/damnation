#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The ship entity class                                              #
#------------------------------------------------------------------------------#

__all__ = ["Character"]

from direct.task import Task
from pandac.PandaModules import NodePath, ActorNode, ForceNode, \
    AngularVectorForce, LinearVectorForce, AngularEulerIntegrator, \
    PointLight, VBase3, VBase4, Point3, CollisionSphere, CollisionNode, \
    Vec3, LRotationf, Quat, Filename
from panda3d.physx import PhysxEnums, PhysxRay, PhysxMask

from Classes.hooks import entity
from base import Base
from Classes.EntityMixins import Damageable, Physical, Container, Carriable
from Classes.bindings import AnalogSlot, DigitalSlot
from Classes.Gui.widget import Text, Progress

@entity
class Character(Base, Physical, Damageable, Container):
    def __post_entity_init__(self, node):
        self.coast = False
        self.headlightsOn = True

        self.slots = {
            'turn': AnalogSlot('turn', posLabel='left', negLabel='right', entity=self),
            'look': AnalogSlot('look', posLabel='up', negLabel='down', entity=self),
            'walk': AnalogSlot('walk', posLabel='forward', negLabel='backward', entity=self),
            'sidestep': AnalogSlot('sidestep', posLabel='left', negLabel='right', entity=self),
            'jump': DigitalSlot('jump', self.jump, entity=self, repeat=True),

            'drop': DigitalSlot('drop', self.drop, entity=self),
            'rotateInventoryUp': DigitalSlot('rotateInventoryUp', self.rotateInventoryUp, entity=self),
            'rotateInventoryDown': DigitalSlot('rotateInventoryDown', self.rotateInventoryDown, entity=self),
            'fire': DigitalSlot('fire', self.fire, entity=self),
        }

        self.angularAxes = ('turn', 'look')
        self.linearAxes = ('sidestep', 'walk')

        self.maximumTargetVelocity = {
            'walk': 150,
            'sidestep': 150,
        }

        self.linearAdjustCoefficient = 500
        self.angularAdjustCoefficient = 1

        self.turnAngle = 0
        self.lookAngle = 0

        self.horizontalLookSensitivity = 18
        self.verticalLookSensitivity = 10

        self.firingTask = None

        self.jumpImpulse = 3000
        self.jumpRay = PhysxRay()
        self.jumpRay.setDirection(Vec3(0, 0, -1))
        self.jumpRay.setLength(2.48)
        self.jumpRayMask = PhysxMask.allOn()
        self.jumpRayMask.clearBit(2)

    def _post_entity_finishedLoading(self):
        self.actor.setGroup(2)
        self.actor.setShapeGroup(2)
        self.actor.setBodyFlag(PhysxEnums.BFFrozenRot, True)
        self.actor.setBodyFlag(PhysxEnums.BFFrozenRotX, True)
        self.actor.setBodyFlag(PhysxEnums.BFFrozenRotY, True)
        self.actor.setBodyFlag(PhysxEnums.BFFrozenRotZ, True)


    ## Actions ##

    def rotateInventoryUp(self, value, *args, **kwargs):
        if value and self.topCarriable() is not None:
            self.rotateUp()

    def rotateInventoryDown(self, value, *args, **kwargs):
        if value and self.topCarriable() is not None:
            self.rotateDown()

    def drop(self, value, *args, **kwargs):
        if value and self.topCarriable() is not None:
            self.removeCarriable(self.topCarriable())

    def fire(self, value, *args, **kwargs):
        if value:
            item = self.topCarriable()
            if item is not None and item.hasAction('fire'):
                # Pretend it's in our hand, since we don't really have that implemented yet.
                item.actor.setGlobalPos(self.node.getPos())
                item.actor.setGlobalHpr(self.node.getH(), self.lookAngle, self.node.getR())
                item.performAction('fire', isInHand=True)

    def jump(self, value, *args, **kwargs):
        if value:
            p = self.actor.getGlobalPos()
            self.jumpRay.setOrigin(p)

            hit = self.contentLoader.getWorld().scene.raycastClosestShape(self.jumpRay, PhysxEnums.STAll, self.jumpRayMask)
            if not hit.isEmpty():
                print "Jump test hit:", hit.getShape().getActor().getName(), "at distance", hit.getDistance()
                self.addImpulse([0, 0, self.jumpImpulse])


    ## Inventory ##

    def _post_entity_addCarriable(self, item):
        print "Added %s to inventory. Hiding." % (str(item), )
        #item.node.reparentTo(self.node)
        #item.node.setPosHpr(self.node, 0, 0, 0, 0, 0, 0)
        item.hide()

    def _post_entity_removeCarriable(self, item):
        print "Removed %s from inventory. Showing." % (str(item), )
        item.show()
        #FIXME: item.actor!
        #item.node.reparentTo(render)
        newPosition = render.getRelativePoint(self.node, VBase3(0, 15, 1))
        item.position.set([newPosition.getX(), newPosition.getY(), newPosition.getZ()])
        item.hpr.set(self.hpr.get())
        print "Moving to position %s. (player: %s)" % (str(newPosition), str(self.position.get()))


    ## GUI ##

    def _entity_getWidget(self, widgetName):
        widget = None
        if widgetName == "Walk":
            widget = Progress("Walk", self)
            widget.setLabel("Walk:")
            widget.setValue(self.getTargetVelocity('walk'))
            widget.setMaxValue(1.0)
            self.widgets[widget] = 'walk'
        elif widgetName == "Sidestep":
            widget = Progress("Sidestep", self)
            widget.setLabel("Sidestep:")
            widget.setValue(self.getTargetVelocity('sidestep'))
            widget.setMaxValue(1.0)
            self.widgets[widget] = 'sidestep'
        elif widgetName == "Forward":
            widget = Text("Forward", self)
            widget.setLabel("Forward:")
            forwardWorld = render.getRelativeVector(self.node, Vec3(0, 1, 0))
            widget.setValue(str(forwardWorld))
            self.widgets[widget] = 'forward'
        return widget

    def _entity_updateWidgets(self, task=None):
        for widget, var in self.widgets.iteritems():
            if var in self.maximumTargetVelocity.keys():
                widget.setValue(self.getTargetVelocity(var))
            elif var in ['damage']:
                widget.setValue(self.strength.get())
            elif var in ['forward']:
                forwardWorld = render.getRelativeVector(self.node, Vec3(0, 1, 0))
                widget.setValue(str(forwardWorld))


    ## Physics and Movement ##

    def _post_entity_collisionStart(self, other, contacts):
        # Hide annoying warnings about the node no longer existing once this player exits.
        #FIXME: Remove if, fix loader.
        if self.node:
            otherBehavior = self.contentLoader.getWorld().getBehavior(other)

            if otherBehavior is not None:
                # Pick up other, if it's carriable.
                if otherBehavior.hasMixin("Carriable"):
                    self.addCarriable(otherBehavior)

    def getMaxTargetVelocity(self, direction):
        return self.maximumTargetVelocity[direction]

    def getTargetVelocity(self, direction):
        return self.slots[direction].getValue()

    def _pre_entity_doMovement(self, stepSize):
        """Determine acceleration from current target velocity settings.

        """
        # Calculate linear acceleration.
        if not self.coast:
            targetLinearVelocityCharacter = [self.slots[axis].getValue() * self.maximumTargetVelocity[axis]
                    for axis in self.linearAxes]
            targetLinearVelocityCharacter.append(0)

            currentLinearVelocityCharacter = self.node.getRelativeVector(render,
                    VBase3(*self.linearVelocity.get()))

            linearAccelerationCharacter = [
                    (targetLinearVelocityCharacter[i] - currentLinearVelocityCharacter[i])
                    * self.linearAdjustCoefficient
                    for i in range(3)]

            # No target velocity in Z!
            linearAccelerationCharacter[2] = 0
            self.actor.addLocalForce(Vec3(*linearAccelerationCharacter))

        # Negative because positive rotation is the opposite of anything you'd expect. (positive is left)
        self.turnAngle += (-self.slots['turn'].getValue() * self.horizontalLookSensitivity)
        while self.turnAngle > 180:
            self.turnAngle -= 360
        while self.turnAngle < -180:
            self.turnAngle += 360
        self.actor.setGlobalHpr(self.turnAngle, 0, 0)

        self.lookAngle += (self.slots['look'].getValue() * self.verticalLookSensitivity)
        self.lookAngle = min(90, max(-90, self.lookAngle))
        base.camera.setP(self.lookAngle)

