#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The explosive prop entity class                                    #
#------------------------------------------------------------------------------#

__all__ = ["ExplosiveProp"]

import math

from direct.task import Task
from pandac.PandaModules import TextureStage

from Classes.hooks import entity
from base import Base
from Classes.EntityMixins import Physical, Damageable

@entity
class ExplosiveProp(Base, Physical, Damageable):
    def __post_entity_init__(self, node):
        pass

    def _post_entity_strengthAtZero(self):
        if self.node is not None:
            self.explosionNode = loader.loadModel('Models/explosion')
            self.explosionNode.setScale(math.sqrt(self.mass.get()))
            self.explosionNode.setPos(self.node.getPos())
            self.explosionNode.reparentTo(render)
            self.explosionNode.setBillboardPointWorld()

            ts = TextureStage('ts')
            ts.setMode(TextureStage.MAdd)

            self.removeExplosionTask = taskMgr.doMethodLater(1, self.removeExplosion, "Remove explosion sprite.")
            self.removeExplosionTask.explosionNode = self.explosionNode

            print "%s: Oh Noes! I'm DEAD! BANG!" % (str(self), )
            self.remove()

    def removeExplosion(self, task):
        task.explosionNode.removeNode()
        print "Explosion node removed."
        return Task.done

