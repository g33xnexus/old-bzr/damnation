#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: Classes for Damnation entities                                     #
#------------------------------------------------------------------------------#

__all__ = ["getEntityClass", "entityClasses"]

entityClasses = {}


import Classes.hooks

def registerEntityClass(cls):
    global entityClasses
    entityClasses[cls.__name__] = cls

Classes.hooks.entity.registerHook(registerEntityClass)


def getEntityClass(name):
    global entityClasses
    if name in entityClasses:
        return entityClasses[name]
    else:
        raise Exception("Unknown entity class: '%s'" % (name, ))


import os

for dirpath, dirnames, filenames in os.walk(os.path.dirname(__file__)):
    prefix = os.path.dirname(os.path.abspath(__file__))
    path = dirpath[len(prefix):].replace("/", ".")

    for filename in filenames:
        if filename.endswith(".py") and filename != "__init__.py":
            if path != "":
                modname = path + "." + filename[:-3]
            else:
                modname = filename[:-3]
            __import__(modname, globals(), locals(), [], 1)

