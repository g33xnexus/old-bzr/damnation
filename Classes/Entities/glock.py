#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The glock entity class                                             #
#------------------------------------------------------------------------------#

__all__ = ["Glock"]

from pandac.PandaModules import VBase3

from Classes.hooks import entity
from base import Base
from Classes.EntityMixins import Physical, Damageable, Usable, Carriable, Weapon

@entity
class Glock(Base, Physical, Damageable, Usable, Carriable, Weapon):
    def __post_entity_init__(self, node):
        self.addAction('fire', self.fire, mustBeInHand=True)

    def __str__(self):
        return self.__class__.__name__

    def fire(self):
        quat = self.actor.getGlobalQuat()
        newPosition = self.actor.getGlobalPos() + (quat.getForward() * 20)
        hpr = quat.getHpr()
        
        bullet = self.contentLoader.loadEntityEgg(
                'Entities/Projectiles/bullet.egg',
                name="Bullet",
                state={'position': (newPosition.getX(), newPosition.getY(), newPosition.getZ()), 'hpr': (hpr.getX(), hpr.getY(), hpr.getZ())})

        #TODO: Muzzle velocity should be 368 m/s for a 9.5g JHP round. (see wikipedia)
        forward = quat.getForward() * 2
        bullet.position.set((newPosition.getX(), newPosition.getY(), newPosition.getZ()))
        bullet.hpr.set((hpr.getX(), hpr.getY(), hpr.getZ()))
        bullet.addImpulse(forward)

