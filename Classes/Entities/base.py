#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The base entity class. All behaviors should inherit from this.     #
#------------------------------------------------------------------------------#
__all__ = ["Base", "Property"]

from direct.showbase.DirectObject import DirectObject
from direct.task import Task

from Classes.configuration import Configuration
from Classes.EntityMixins import isMixin, entityMixinClasses
from Classes.entity_property import Property


class Base(object, DirectObject):

    lastID = 0

    (
        ServerOnly,
        OwnerOnly,
        All
    ) = range(3)

    def __init__(self, node):
        "Create a behavior for the given node."
        Base.lastID += 1
        self.ID = Base.lastID
        self.node = node
        self.lastState = dict()
        self.definitionFilename = None
        self.slots = dict()
        self.widgets = dict()
        self.widgetUpdateStarted = False
        self.enabled = True

        self.position = self.createProperty('position', [0, 0, 0])
        self.position.onChange.append(self._setPosition)
        self.hpr = self.createProperty('hpr', [0, 0, 0])
        self.hpr.onChange.append(self._setHpr)
        self.metadata = self.createProperty('metadata', dict())

        self.node.reparentTo(render)
        self.parent = self.createProperty('parent', None)
        self.parent.onChange.append(self.parentChanged)

        self.check_mixins()
        self.call_mixin_method('__entity_init__', node)

    def __post_entity_init__(self, node):
        """Override this instead of __init__.

        """
        pass

    def hide(self):
        if self.node is not None:
            self.node.hide()
            self.call_mixin_method('_entity_hide')
            self.node.stash()
            self.enabled = False

    def show(self):
        if self.node is not None:
            self.node.unstash()
            self.call_mixin_method('_entity_show')
            self.node.show()
            self.enabled = True

    def check_mixins(self):
        thisClass = self.__class__

        self._mixins = list()

        # Check mixin dependencies and call init methods.
        classesToCheck = [thisClass]
        for cls in classesToCheck:
            if isMixin(cls):
                dependenciesMet = True

                if cls.__dict__.has_key('mixin_dependencies'):
                    for dep in cls.__dict__['mixin_dependencies']:
                        if dep not in thisClass.__bases__:
                            print 'ERROR: Missing dependency "%s" for entity mixin "%s"!' % (repr(dep), repr(cls))
                            dependenciesMet = False

                if dependenciesMet:
                    self._mixins.append(cls)

            classesToCheck.extend(cls.__bases__)

    def call_mixin_method(self, methodName, *args):
        thisClass = self.__class__
        returns = list()

        if methodName.startswith("__"):
            prefix = "__"
            strippedMethodName = methodName[2:]
        elif methodName.startswith("_"):
            prefix = "_"
            strippedMethodName = methodName[1:]
        else:
            prefix = ""
            strippedMethodName = methodName

        # Run "pre" method
        if prefix + "pre_" + strippedMethodName in dir(self):
            getattr(self, prefix + "pre_" + strippedMethodName)(*args)

        # Check mixin dependencies and call init methods.
        classesToIgnore = set()
        classesToCheck = [thisClass]
        for cls in self._mixins:
            if cls in classesToIgnore:
                # Ignore their bases too.
                for base in cls.__bases__:
                    classesToIgnore.add(base)
            else:
                if cls.__dict__.has_key(methodName):
                    returns.append(cls.__dict__[methodName](self, *args))
                    for base in cls.__bases__:
                        classesToIgnore.add(base)

        # Run "post" method
        if prefix + "post_" + strippedMethodName in dir(self):
            getattr(self, prefix + "post_" + strippedMethodName)(*args)

        return returns

    def mixins(self):
        return [str(cls.__name__) for cls in self._mixins]

    def hasMixin(self, name):
        return name in self.mixins()

    def __str__(self):
        return "%s(%s)" % (self.__class__.__name__, self.node)

    def __unicode__(self):
        return str(self)

    def _setPosition(self, prop):
        self.node.setPos(*self.position.get())
        self.call_mixin_method('_entity_setPosition', prop)

    def _setHpr(self, prop):
        self.node.setHpr(*self.hpr.get())
        self.call_mixin_method('_entity_setHpr', prop)

    def getWidget(self, widgetName):
        self.startWidgetUpdate()
        for widget in self.call_mixin_method('_entity_getWidget', widgetName):
            if widget is not None:
                return widget
        return None

    def startWidgetUpdate(self):
        if not self.widgetUpdateStarted:
            taskMgr.doMethodLater(float(Configuration['gui_update_interval']), self.updateWidgets, "Update the ship HUD widgets")
            self.widgetUpdateStarted = True

    def updateWidgets(self, task=None):
        self.call_mixin_method('_entity_updateWidgets', task)
        return Task.again

    def finishedLoading(self):
        self.call_mixin_method('_entity_finishedLoading')

    def parentChanged(self, prop):
        if self.parent.get() is None:
            self.node.reparentTo(render)
        else:
            if self.parent.get().node is not None:
                self.node.reparentTo(self.parent.get().node)
            else:
                print "WARNING: Parent's node is None. Did they sign off?"

    def setState(self, state, timestamp=None):
        """Set this entity's state.

        state should be a dict, where keys correspond to individual state
            elements.

        """
        self.position.setFromState(state, timestamp)
        self.hpr.setFromState(state, timestamp)
        self.parent.setFromState(state, None)

        self.call_mixin_method('_entity_setState', state, timestamp)

    def getState(self):
        """Get this entity's state.

        Returns a dict, where keys correspond to individual state elements.

        """
        pos = self.node.getPos()
        pos = (pos[0], pos[1], pos[2])
        self.position.value = pos

        hpr = self.node.getHpr()
        hpr = (hpr[0], hpr[1], hpr[2])
        self.hpr.value = hpr

        state = {}
        self.position.addToState(state)
        self.hpr.addToState(state)
        self.parent.addToState(state)

        otherStates = self.call_mixin_method('_entity_getState')
        for otherState in otherStates:
            state.update(otherState)

        return state

    def getStateDelta(self):
        """Get the elements of this entity's state that have changed since the
        last call to getStateDelta().

        """
        newState = self.getState()
        delta = dict()

        # Check for new keys and values that have changed.
        for key, val in newState.iteritems():
            if key not in self.lastState \
                    or self.lastState[key] != val:
                delta[key] = val

        # Check for keys that are no longer in the state.
        for key in self.lastState:
            if key not in newState:
                delta[key] = None

        self.lastState = newState
        return delta

    def getSlots(self):
        return self.slots

    def event(self, eventName, *args, **kwargs):
        if eventName in self.slots:
            self.slots[eventName](*args, **kwargs)
        else:
            print "Warning: Couldn't find event slot '%s'! Ignoring. (available slots: %s)" % (eventName, self.slots)

    def createProperty(self, tag, initial=None):
        return Property(tag, self, initial)

    def getNetworkPropagation(self):
        return All

    def getChildren(self):
        return ()

    def remove(self):
        if self.node is not None:
            self.call_mixin_method('_entity_remove')
        else:
            print "Tried to remove \"%s\" twice!" % (str(self), )

    def _post_entity_remove(self):
        print "Removing node", self.node
        self.node.removeNode()
        self.node = None

