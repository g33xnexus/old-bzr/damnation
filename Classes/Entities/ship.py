#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The ship entity class                                              #
#------------------------------------------------------------------------------#

__all__ = ["Ship"]

from direct.task import Task
from pandac.PandaModules import NodePath, ActorNode, ForceNode, \
    AngularVectorForce, LinearVectorForce, AngularEulerIntegrator, \
    PointLight, VBase3, VBase4, Point3, CollisionSphere, CollisionNode, \
    Vec3, LRotationf, Quat, Filename
from direct.particles.ParticleEffect import ParticleEffect

from Classes.hooks import entity
from base import Base
from Classes.EntityMixins import System
from Classes.bindings import AnalogSlot, DigitalSlot
from Classes.Gui.widget import Text, Progress

@entity
class Ship(Base, System):
    def __post_entity_init__(self, node):
        self.coast = False
        self.headlightsOn = True

        self.slots = {
            'yaw': AnalogSlot('yaw', posLabel='left', negLabel='right', entity=self),
            'pitch': AnalogSlot('pitch', posLabel='up', negLabel='down', entity=self),
            'roll': AnalogSlot('roll', posLabel='right', negLabel='left', entity=self),
            'throttle': AnalogSlot('throttle', posLabel='left', negLabel='right', entity=self),
            'lift': AnalogSlot('lift', posLabel='left', negLabel='right', entity=self),
            'slide': AnalogSlot('slide', posLabel='left', negLabel='right', entity=self),
            'headlights': DigitalSlot('headlights', self.setHeadlights, entity=self),
            'coast': DigitalSlot('coast', self.setCoast, entity=self),

            #TODO: Remove (debugging code)
            'damagekinetic': DigitalSlot('damagekinetic', self.damageKinetic, entity=self),
            'damagethermal': DigitalSlot('damagethermal', self.damageThermal, entity=self),
            'damageem': DigitalSlot('damageem', self.damageEM, entity=self),
            'firesphere': DigitalSlot('firesphere', self.fireSphere, entity=self),
        }

        self.angularAxes = ('yaw', 'pitch', 'roll')
        self.linearAxes = ('slide', 'throttle', 'lift')

        self.maximumTargetVelocity = {
            'yaw': 1,
            'pitch': 1,
            'roll': 1,
            'throttle': 30,
            'lift': 12,
            'slide': 12,
        }

        headlights = self.node.findAllMatches("headlight")
        self.headlights = [headlights[x] for x in range(headlights.getNumPaths())]

        self.throttleAdjustSpeed = 60

        self.linearAdjustCoefficient = 5
        self.angularAdjustCoefficient = 1

        self.accept('into', self.handleCollision)
        self.accept('again', self.handleAgain)

        self.firingTask = None

        self.strength.onChange.append(self._setStrength)

#        self.damagedParticles = ParticleEffect()
#        self.damagedParticles.loadConfig(Filename('Models/Particles/greensmoke.ptf'))
#        self.damagedParticles.start(self.node, render)
#        self.damagedParticles.setPos(0, -38, 0)
#        #self.damagedParticles.setScale(1.5, 1.5, 1.5)
#        self.damagedParticles.setLightOff()
#        #self.damagedParticles.disable()
#        self.damagedParticles.reparentTo(self.node)

    def _setStrength(self, prop):
        return
        if self.strength.get() < self.maxStrength.get() / 10.0:
            self.damagedParticles.enable()
        else:
            self.damagedParticles.disable()

    def handleAgain(self, entry):
        self.handleCollision(entry)

    def handleCollision(self, entry):
        # Hide annoying warnings about the node no longer existing once this player exits.
        #FIXME: Remove if, fix loader.
        if self.node:
            self.doDamage({'kinetic':5, 'thermal': 0, 'em': 0})

    def _entity_getWidget(self, widgetName):
        widget = None
        if widgetName == "Throttle":
            widget = Progress("Throttle", self)
            widget.setLabel("Throttle:")
            widget.setValue(self.getThrottle('throttle'))
            widget.setMaxValue(1.0)
            self.widgets[widget] = 'throttle'
        elif widgetName == "Lift":
            widget = Progress("Lift", self)
            widget.setLabel("Lift:")
            widget.setValue(self.getThrottle('lift'))
            widget.setMaxValue(1.0)
            self.widgets[widget] = 'lift'
        elif widgetName == "Slide":
            widget = Progress("Slide", self)
            widget.setLabel("Slide:")
            widget.setValue(self.getThrottle('slide'))
            widget.setMaxValue(1.0)
            self.widgets[widget] = 'slide'
        elif widgetName == "Forward":
            widget = Text("Forward", self)
            widget.setLabel("Forward:")
            forwardWorld = render.getRelativeVector(self.node, Vec3(0, 1, 0))
            widget.setValue(str(forwardWorld))
            self.widgets[widget] = 'forward'
        return widget

    def _entity_updateWidgets(self, task=None):
        for widget, var in self.widgets.iteritems():
            if var in ['yaw', 'pitch', 'roll', 'throttle', 'lift', 'slide']:
                widget.setValue(self.getThrottle(var))
            elif var in ['damage']:
                widget.setValue(self.strength.get())
            elif var in ['forward']:
                forwardWorld = render.getRelativeVector(self.node, Vec3(0, 1, 0))
                widget.setValue(str(forwardWorld))

    def damageKinetic(self, value, *args, **kwargs):
        #TODO: Remove (debugging code)
        if value:
            self.doDamage({'kinetic': 20, 'thermal': 0, 'em': 0})

    def damageThermal(self, value, *args, **kwargs):
        #TODO: Remove (debugging code)
        if value:
            self.doDamage({'kinetic': 0, 'thermal': 20, 'em': 0})

    def damageEM(self, value, *args, **kwargs):
        #TODO: Remove (debugging code)
        if value:
            self.doDamage({'kinetic': 0, 'thermal': 0, 'em': 20})

    def setHeadlights(self, value, *args, **kwargs):
        self.headlightsOn = value
        if self.headlights is not None:
            for light in self.headlights:
                target = light.getPythonTag("_lightTarget")
                if self.headlightsOn:
                    target.setLight(light)
                else:
                    target.setLightOff(light)

    def setCoast(self, value, *args, **kwargs):
        self.coast = value

    def fireSphere(self, value, *args, **kwargs):
        if value and self.firingTask is None:
            self.doFireSphere()
            self.firingTask = "NOT NONE"
            #self.firingTask = taskMgr.doMethodLater(1, self.doFireSphere, "Fire spheres.")
        elif not value and self.firingTask is not None:
            #self.firingTask.remove()
            self.firingTask = None

    def doFireSphere(self, task=None):
        print "Fire Zeh Missiles!"
        newPosition = self.node.getRelativeVector(render, VBase3(-10, 120, 0))
        newPosition = (newPosition.getX(), newPosition.getY(), newPosition.getZ())

        orientationQuat = Quat(self.body.getQuaternion())
        hpr = orientationQuat.getHpr()

        sphere = self.contentLoader.loadEntityEgg(
                'Entities/Projectiles/sphere.egg',
                name="Sphere",
                state={'position': newPosition, 'hpr': (hpr.getX(), hpr.getY(), hpr.getZ())})

        sphere.addLocalImpulse(orientationQuat.getForward() * 500)
        return Task.again


    def getMaxThrottle(self, direction):
        return self.maximumTargetVelocity[direction]

    def getThrottle(self, direction):
        return self.slots[direction].getValue()

    def _entity_doMovement(self, stepSize):
        """Determine acceleration from current target velocity settings.

        """
        # Calculate angular acceleration.
        targetAngularVelocityShip = [self.slots[axis].getValue() * self.maximumTargetVelocity[axis]
            for axis in self.angularAxes]

        # ODE seems to have weird ideas about what yaw, pitch, and roll are. (it uses pitch/roll/yaw)
        targetAngularVelocityShip = [targetAngularVelocityShip[1], targetAngularVelocityShip[2], targetAngularVelocityShip[0]]

        currentAngularVelocityShip = self.angularVelocity.get()

        angularAccelerationShip = [
            (targetAngularVelocityShip[i] - currentAngularVelocityShip[i])
            * self.angularAdjustCoefficient
            for i in range(3)]

        self.setAngularForce(*[angularAccelerationShip[i] * self.mass.get() for i in range(3)])

        # Calculate linear acceleration.
        if not self.coast:
            targetLinearVelocityShip = [self.slots[axis].getValue() * self.maximumTargetVelocity[axis]
                    for axis in self.linearAxes]

            currentLinearVelocityShip = self.node.getRelativeVector(render,
                    VBase3(*self.linearVelocity.get()))

            linearAccelerationShip = [
                    (targetLinearVelocityShip[i] - currentLinearVelocityShip[i])
                    * self.linearAdjustCoefficient
                    for i in range(3)]

            linearAccelerationWorld = render.getRelativeVector(self.node,
                    VBase3(*linearAccelerationShip))
            self.setLinearForce(*[linearAccelerationWorld[i] * self.mass.get() for i in range(3)])

        else:
            self.setLinearForce(0, 0, 0)

    def _entity_setState(self, state, timestamp=None):
        """Set this entity's state.

        state should be a dict, where keys correspond to individual state
            elements.

        """
        for key, val in state.iteritems():
            if key == 'coast':
                self.coast = val
            elif key == 'headlights':
                self.setHeadlights(val)

    def _entity_getState(self):
        """Get this entity's state.

        Returns a dict, where keys correspond to individual state elements.

        """
        state['coast'] = self.coast
        state['headlights'] = self.headlightsOn
        return state

