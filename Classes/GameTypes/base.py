#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The base game type class                                           #
#------------------------------------------------------------------------------#

from direct.showbase.DirectObject import DirectObject

# Panda3D imports
from pandac.PandaModules import WindowProperties, PStatClient
from direct.filter.CommonFilters import CommonFilters

# Python imports
import sys, os

# Local imports
from Classes.configuration import Configuration
from Classes.loader import ContentLoader
from Classes.Gui import layout, theme
from Classes.Gui.widget import widgetProvider

class Base(DirectObject, object):
    def __init__(self, skip_window=False):
        if not skip_window:
            # Get a reference to our window
            self.mainWindow = base.win

            # Turn on an fps meter
            if Configuration['showFPSMeter']:
                base.setFrameRateMeter(True)

            # Set the background color to bright magenta so we can see leaks.
            base.setBackgroundColor(1, 0, 1, 1)

            # Use class 'CommonFilters' to enable a bloom filter.
            # The brightness of a pixel is measured using a weighted average
            # of R,G,B,A.  We put all the weight on Alpha, meaning that for
            # us, the framebuffer's alpha channel alpha controls bloom.
            self.filters = CommonFilters(base.win, base.cam)
            glowSize = Configuration['glowFilterSize']
            if glowSize is None:
                glowSize = "small"
            if glowSize != "off":
                filterok = self.filters.setBloom(
                    blend=(0, 0, 0, 0.9),
                    desat=0.1,
                    intensity=4.0,
                    size=glowSize
                )
                if (filterok == False):
                    print "Warning: Video card not powerful enough to enable " + \
                        "glow filter; disabling."

            # Set the Main Window's properties
            winProp = WindowProperties()
            winProp.setTitle("Requiem for Innocence: Damnation Technology Demo")
            self.mainWindow.requestProperties(winProp)

            # Enable particles.
            base.enableParticles()

        # Enable PStats if needed
        if Configuration['enablePStats']:
            PStatClient.connect()

    def startLevel(self):
        # Load the level
        pass

