#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The base game type class                                           #
#------------------------------------------------------------------------------#

from direct.showbase.DirectObject import DirectObject

# Panda3D imports
from pandac.PandaModules import WindowProperties, PStatClient
from direct.filter.CommonFilters import CommonFilters

# Python imports
import sys, os

# Local imports
from base import Base
from Classes.configuration import Configuration
from Classes.loader import ContentLoader
from Classes.inputmanager import InputManager
from Classes.Gui import layout, theme
from Classes.Gui.widget import widgetProvider
from Classes.window import Window

class Standalone(Base):
    def __init__(self):
        super(Standalone, self).__init__()

        self.controlledEntity = None
        self.input = None

        #TODO: Generate character info.
        self.charInfo = None

    def startLevel(self):
        # Load the level
        ContentLoader.loadLevelEgg(Configuration['masterBaseLevel'])

        startpos = ContentLoader.getStartPosition()

        if startpos is None:
            print "No start position found!"
            state = {}

        else:
            state = {
                'position': startpos[0],
                'hpr': startpos[1],
            }

        entity = ContentLoader.loadEntityEgg(
                Configuration['playerEntityFile'],
                state=state, loadSubEntities=True)
        self.setControlledEntity(entity)

    def setControlledEntity(self, entity):
        if self.controlledEntity is not None:
            raise "Re-assigning controlled entity?!"

        self.controlledEntity = entity

        # Set up the player
        self.input = InputManager(entity)

        print "Assigning controlled entity to", entity.node.getName()

        # Set up the camera.
        base.disableMouse() # GIMME MY CAMERA BACK!
        base.camera.reparentTo(entity.node)
        cameraPos = [float(x) for x in Configuration['cameraPos'].split(',')]
        base.camera.setPos(*cameraPos)
        base.camLens.setFar(100000)
        base.camLens.setFov(60)

        # Load GUI layout and theme
        widgetProvider(entity, "Player")
        currentTheme = theme.Theme("default")
        layout.manager.setTheme(currentTheme)
        layout.manager.loadLayout("Hud/default.xml")

