#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The base game type class                                           #
#------------------------------------------------------------------------------#

from direct.showbase.DirectObject import DirectObject

# Panda3D imports
from pandac.PandaModules import WindowProperties, PStatClient
from direct.filter.CommonFilters import CommonFilters

# Python imports
import sys, os

# Local imports
from base import Base
from Classes.configuration import Configuration
from Classes.loader import ContentLoader
from Classes.Gui import layout, theme
from Classes.Gui.widget import widgetProvider
import Classes.Networking.server as networkServer

class Server(Base):
    def __init__(self):
        super(Server, self).__init__(Configuration['dedicated'])

        # Create the network server and start listening for connections.
        self.server = networkServer.Server(Configuration['listenPort'], compress=True)

    def startLevel(self):
        # Load the level
        ContentLoader.loadLevelEgg(Configuration['masterBaseLevel'])

