#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The base game type class                                           #
#------------------------------------------------------------------------------#

from direct.showbase.DirectObject import DirectObject

# Panda3D imports
from pandac.PandaModules import WindowProperties, PStatClient
from direct.filter.CommonFilters import CommonFilters

# Python imports
import sys, os

# Local imports
from standalone import Standalone
from Classes.configuration import Configuration
from Classes.loader import ContentLoader
from Classes.Gui import layout, theme
from Classes.Gui.widget import widgetProvider
import Classes.Networking.client as networkClient

class Client(Standalone):
    def __init__(self):
        super(Client, self).__init__()

        # Instantiate a network client object.
        self.netClient = networkClient.Client(self.charInfo, Configuration['server'], Configuration['port'],
            setControlledEntity=self.setControlledEntity)

    def startLevel(self):
        # Load the level
        ContentLoader.loadLevelEgg(Configuration['clientBaseLevel'])

