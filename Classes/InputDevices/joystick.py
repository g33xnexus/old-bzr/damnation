#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The joystick input class.                                          #
#------------------------------------------------------------------------------#

# Panda imports
from direct.task import Task
from direct.gui.OnscreenText import OnscreenText
from pandac.PandaModules import TextNode

# Local imports
from Classes.configuration import Configuration
from Classes.bindings import AxisSignal, ButtonSignal, KeyState


__all__ = ['Joystick']


try:
    # PyGame imports
    import pygame
    pygame.init()
    from pygame import joystick


    class Joystick:
        class JoystickDevice:
            def __init__(self, joynum):
                self.pollCallback = None
                self.polling = False
                self.signalsEnabled = False
                self.axes = {}
                self.buttons = {}

                self.joynum = joynum
                js = joystick.Joystick(joynum)
                if js.get_init():
                    print "Warning: Joystick %d already initialized; skipping!" % (joynum, )
                    return
                self.name = js.get_name()

                self.__joystick = js
                print "Setting up joystick '%s'." % (js.get_name(), )
                js.init()

                for n in range(self.__joystick.get_numbuttons()):
                    buttonName = 'button' + str(n)
                    buttonSignal = ButtonSignal(buttonName, "Button " + str(n))
                    self.buttons[buttonName] = buttonSignal

                for n in range(self.__joystick.get_numaxes()):
                    axisName = 'axis' + str(n)
                    axisSignal = AxisSignal(axisName, "Axis " + str(n))
                    self.axes[axisName] = axisSignal

                for n in range(self.__joystick.get_numhats()):
                    axisNameX = 'hat' + str(n) + "x"
                    axisSignalX = AxisSignal(axisNameX, "Hat " + str(n) + " horizontal")
                    self.axes[axisNameX] = axisSignalX

                    axisNameY = 'hat' + str(n) + "y"
                    axisSignalY = AxisSignal(axisNameY, "Hat " + str(n) + " vertical")
                    self.axes[axisNameY] = axisSignalY

                for n in range(self.__joystick.get_numballs()):
                    axisNameX = 'trackball' + str(n) + "x"
                    axisSignalX = AxisSignal(axisNameX, "Trackball " + str(n) + " horizontal")
                    self.axes[axisNameX] = axisSignalX

                    axisNameY = 'trackball' + str(n) + "y"
                    axisSignalY = AxisSignal(axisNameY, "Trackball " + str(n) + " vertical")
                    self.axes[axisNameY] = axisSignalY

                #TODO: Remove the rest of this method?
                if Configuration['showJoystickDebug'] or Configuration['debugMode']:
                    self.setupDebugOutput()

            def getAxes(self):
                return self.axes.keys()

            def getSignal(self, name):
                if name in self.buttons:
                    return self.buttons[name]
                elif name in self.axes:
                    return self.axes[name] 
                else:
                    # We don't know what this signal is.
                    print "Warning: Unknown joystick signal: " + name
                    return None

            def startButtonPolling(self, callback):
                self.pollCallback = callback

                if self.signalsEnabled:
                    raise "Error: " + str(self) + " tried to start polling with signals enabled!"
                if self.polling:
                    raise "Error: " + str(self) + " already polling for buttons!"

                self.polling = True

                for button, name in self.buttons.iteritems():
                    base.accept(button, self.capture, [button, name])

            def stopButtonPolling(self):
                if self.polling:
                    for button in self.buttons:
                        base.ignore(button)
                    self.polling = False
                else:
                    print "Warning: " + str(self) + " tried to stop polling without starting first."

            def capture(self, buttonName):
                print buttonName
                self.stopButtonPolling()
                if self.pollCallback is not None:
                    self.pollCallback(self.getSignal(buttonName))

            def enableSignals(self):
                if self.polling:
                    raise "Error: " + str(self) + " tried to enable signals while polling!"
                self.signalsEnabled = True
                joyname = "joystick%d-" % self.joynum
                for name, signal in self.buttons.iteritems():
                    base.accept(joyname + name, signal, [KeyState.Press])
                    base.accept(joyname + name + '-up', signal, [KeyState.Release])
                for name, signal in self.axes.iteritems():
                    base.accept(joyname + name, signal)

            def disableSignals(self):
                joyname = "joystick%d-" % self.joynum
                if self.signalsEnabled:
                    for name, signal in self.buttons.iteritems():
                        base.ignore(joyname + name)
                        base.ignore(joyname + name + '-up')
                    for name, signal in self.axes.iteritems():
                        base.ignore(joyname + name)
                    self.signalsEnabled = False
                else:
                    print "Warning: " + str(self) + " tried to disable signals when they weren't enabled."

            def setupDebugOutput(self):
                text = "Joystick %d (%s)\n" % (self.joynum, self.name)
                for idx in range(self.__joystick.get_numbuttons()):
                    text += "  Button %d: %s\n" % (idx + 1, str(self.__joystick.get_button(idx)))
                for idx in range(self.__joystick.get_numaxes()):
                    text += "  Axis %d: %s" % (idx + 1, str(self.__joystick.get_axis(idx)))
                self.debugtext = OnscreenText(text=text, style=1, fg=(1, 1, 1, 1),
                    pos=(-1.3, 0.95), align=TextNode.ALeft, scale=.05, mayChange=True)
                self.debugtext.flattenLight()
                taskMgr.add(self.debugOutput, "Display debugging output.")

            def debugOutput(self, task):
                "Generate debugging output for this joystick."
                text = "Joystick %d (%s)\n" % (self.joynum, self.name)
                for idx in range(self.__joystick.get_numbuttons()):
                    text += "  Button %d: %s\n" % (idx + 1, str(self.__joystick.get_button(idx)))
                for idx in range(self.__joystick.get_numaxes()):
                    text += "  Axis %d: %s\n" % (idx + 1, str(self.__joystick.get_axis(idx)))
                self.debugtext.setText(text)

                return Task.cont

            def printInfo(self):
                "Print out useful information about this joystick or gamepad."
                print "  Number of buttons: " + str(self.__joystick.get_numbuttons())
                print "  Number of axes: " + str(self.__joystick.get_numaxes())
                print "  Number of POV hats: " + str(self.__joystick.get_numhats())
                print "  Number of trackballs: " + str(self.__joystick.get_numballs())


        def __init__(self):
            # Initialize all joystick devices.
            self.devices = []
            for joynum in range(joystick.get_count()):
                self.devices.append(Joystick.JoystickDevice(joynum))

            taskMgr.add(self._joystickEvents, "Process joystick events.")

        def getDevices(self):
            return self.devices

        def _joystickEvents(self, task):
            "Process all PyGame events."
            for e in pygame.event.get():
                if e.type == pygame.JOYAXISMOTION:
                    eventName = "joystick%d-axis%d" % (e.dict["joy"], e.dict["axis"])
                    messenger.send(eventName, [e.dict["value"]])

                elif e.type == pygame.JOYBALLMOTION:
                    eventName = "joystick%d-trackball%dx" % (e.dict["joy"], e.dict["ball"])
                    messenger.send(eventName, [e.dict["rel"][0]])
                    eventName = "joystick%d-trackball%dy" % (e.dict["joy"], e.dict["ball"])
                    messenger.send(eventName, [e.dict["rel"][1]])

                elif e.type == pygame.JOYHATMOTION:
                    eventName = "joystick%d-hat%dx" % (e.dict["joy"], e.dict["hat"])
                    messenger.send(eventName, [e.dict["value"][0]])
                    eventName = "joystick%d-hat%dy" % (e.dict["joy"], e.dict["hat"])
                    messenger.send(eventName, [e.dict["value"][1]])

                elif e.type == pygame.JOYBUTTONDOWN:
                    eventName = "joystick%d-button%d" % (e.dict["joy"], e.dict["button"])
                    messenger.send(eventName, [])

                elif e.type == pygame.JOYBUTTONUP:
                    eventName = "joystick%d-button%d-up" % (e.dict["joy"], e.dict["button"])
                    messenger.send(eventName, [])

            return Task.cont


except ImportError:
    print "Warning: pygame.joystick not found. No joystick support!"

    class Joystick:
        def getDevices(self):
            return []

Joystick = Joystick()

