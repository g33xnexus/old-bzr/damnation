#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The Keyboard device classes.                                       #
#------------------------------------------------------------------------------#

# Panda imports
from pandac.PandaModules import ModifierButtons

# Local imports
from Classes.bindings import AxisSignal, ButtonSignal, KeyState


__all__ = ['Keyboard']


class Keyboard:
    aliases = {
        'backtick': '`',
        'tilde': '~',

        'minus': '-',
        'dash': '-',
        'plus': '+',
        'equal': '=',
        'equals': '=',

        'left bracket': '[',
        'open bracket': '[',
        'right bracket': ']',
        'close bracket': ']',
        'backslash': '\\',
        'semicolon': ';',

        "apostrophe": "'",
        "single quote": "'",
        'comma': ',',
        'period': '.',
        'dot': '.',
        'slash': '/',
        }

    buttons = {
        'a': 'A',
        'b': 'B',
        'c': 'C',
        'd': 'D',
        'e': 'E',
        'f': 'F',
        'g': 'G',
        'h': 'H',
        'i': 'I',
        'j': 'J',
        'k': 'K',
        'l': 'L',
        'm': 'M',
        'n': 'N',
        'o': 'O',
        'p': 'P',
        'q': 'Q',
        'r': 'R',
        's': 'S',
        't': 'T',
        'u': 'U',
        'v': 'V',
        'w': 'W',
        'x': 'X',
        'y': 'Y',
        'z': 'Z',

        '`': '~',
        '0': '0',
        '1': '1',
        '2': '2',
        '3': '3',
        '4': '4',
        '5': '5',
        '6': '6',
        '7': '7',
        '8': '8',
        '9': '9',
        '-': '-',
        '=': '=',

        '[': '[',
        ']': ']',
        '\\': '\\',
        ';': ';',

        "'": "'",
        ',': ',',
        '.': '.',
        '/': '/',

        'f1': 'F1',
        'f2': 'F2',
        'f3': 'F3',
        'f4': 'F4',
        'f5': 'F5',
        'f6': 'F6',
        'f7': 'F7',
        'f8': 'F8',
        'f9': 'F9',
        'f10': 'F10',
        'f11': 'F11',
        'f12': 'F12',

        'enter': 'Enter',
        'tab': 'Tab',
        'space': 'Space',

        'escape': 'Escape',
        'backspace': 'Backspace',

        'insert': 'Insert',
        'delete': 'Delete',

        'home': 'Home',
        'end': 'End',
        'page_up': 'Page Up',
        'page_down': 'Page Down',

        'arrow_left': 'Arrow Left',
        'arrow_up': 'Arrow Up',
        'arrow_down': 'Arrow Down',
        'arrow_right': 'Arrow Right',

        'num_lock': 'Num Lock',
        'caps_lock': 'Caps Lock',
        'scroll_lock': 'Scroll_lock',

        'lshift': 'Left Shift',
        'rshift': 'Right Shift',
        'lcontrol': 'Left Control',
        'rcontrol': 'Right Control',
        'lalt': 'Left Alt',
        'ralt': 'Right Alt',
        }

    class KeyboardDevice:
        def __init__(self):
            self.pollCallback = None
            self.polling = False
            self.signalsEnabled = False
            self.signals = {}
            self.name = "Keyboard"

            # Turn off compound events so we can actually use the modifier keys
            # as expected in a game.
            base.mouseWatcherNode.setModifierButtons(ModifierButtons())
            base.buttonThrowers[0].node().setModifierButtons(ModifierButtons())

        def getAxes(self):
            return ()

        def getSignal(self, name):
            if name in Keyboard.aliases:
                name = Keyboard.aliases[name]
            if name in self.signals:
                # We've already created a Signal object for this signal.
                return self.signals[name]
            elif name in Keyboard.buttons:
                sig = ButtonSignal(name, Keyboard.buttons[name])
                self.signals[name] = sig
                return sig
            else:
                # We don't know what this signal is.
                print "Warning: Unknown keyboard signal: " + name
                return None

        def startButtonPolling(self, callback):
            self.pollCallback = callback

            if self.signalsEnabled:
                raise "Error: " + str(self) + " tried to start polling with signals enabled!"
            if self.polling:
                raise "Error: " + str(self) + " already polling for buttons!"

            self.polling = True

            for key, name in Keyboard.buttons.iteritems():
                base.accept(key, self.capture, [key, name])

        def stopButtonPolling(self):
            if self.polling:
                for key in Keyboard.buttons:
                    base.ignore(key)
                self.polling = False
            else:
                print "Warning: " + str(self) + " tried to stop polling without starting first."

        def capture(self, keyName):
            print keyName
            self.stopButtonPolling()
            if self.pollCallback is not None:
                self.pollCallback(self.getSignal(keyName))

        def enableSignals(self):
            if self.polling:
                raise "Error: " + str(self) + " tried to enable signals while polling!"
            self.signalsEnabled = True
            for name, signal in self.signals.iteritems():
                base.accept(name, signal, [KeyState.Press])
                base.accept(name + '-up', signal, [KeyState.Release])
                base.accept(name + '-repeat', signal, [KeyState.Repeat])

        def disableSignals(self):
            if self.signalsEnabled:
                for name, signal in self.signals.iteritems():
                    base.ignore(name)
                    base.ignore(name + '-up')
                    base.ignore(name + '-repeat')
                self.signalsEnabled = False
            else:
                print "Warning: " + str(self) + " tried to disable signals when they weren't enabled."

        def printInfo(self):
            "Print out useful information about the keyboard. (not much here)"
            pass


    def __init__(self):
        self.devices = [Keyboard.KeyboardDevice()]

    def getDevices(self):
        return self.devices

Keyboard = Keyboard()

