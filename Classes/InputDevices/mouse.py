#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The Mouse device classes.                                          #
#------------------------------------------------------------------------------#

# Panda imports
from pandac.PandaModules import WindowProperties
from direct.task import Task

# Local imports
from Classes.bindings import AxisSignal, ButtonSignal, KeyState


__all__ = ['Mouse']


class Mouse:
    buttons = {
        'mouse1': 'Button 1',
        'mouse2': 'Button 2',
        'mouse3': 'Button 3',
        'wheel_up': 'Wheel Up',
        'wheel_down': 'Wheel Down',
    }

    class MouseDevice:
        def __init__(self, mouseIndex, mouseWatcher):
            self.pollCallback = None
            self.polling = False
            self.signalsEnabled = False
            self.axisSignals = {}
            self.buttonSignals = {}

            self.name = mouseWatcher.getName().replace('watcher', 'mouse ')
            self.mouseIndex = mouseIndex
            self.mouseWatcher = mouseWatcher
            self.mousePrefix = ""
            if self.mouseIndex > 0:
                self.mousePrefix = "mousedev%d-" % (self.mouseIndex, )

            for axis in ['x', 'y']:
                axisSignal = AxisSignal(axis, "%s Axis" % (axis.upper()))
                self.axisSignals[axis] = axisSignal

            self.checkAxisTask = None

            self.enable()

        def tskCheckAxis(self, task):
            if self.signalsEnabled:
                if self.mouseWatcher.hasMouse():
                    dt = globalClock.getDt()
                    center = (base.win.getXSize() / 2, base.win.getYSize() / 2)

                    #FIXME: Will this fail on a raw mouse?
                    pos = [self.mouseWatcher.getMouseX(), self.mouseWatcher.getMouseY()]
                    if abs(pos[0]) < (2.0 / base.win.getXSize()):
                        pos[0] = 0
                    if abs(pos[1]) < (2.0 / base.win.getYSize()):
                        pos[1] = 0

                    self.axisSignals['x'](pos[0] / dt)
                    self.axisSignals['y'](pos[1] / dt)

                    base.win.movePointer(self.mouseIndex, *center)
                    base.win.movePointer(0, *center)
                else:
                    print "No mouse yet!"
            return Task.cont

        def getAxes(self):
            return self.axisSignals.keys()

        def getSignal(self, name):
            if name in self.axisSignals:
                # We've already created a Signal object for this axis signal.
                return self.axisSignals[name]
            elif name in self.buttonSignals:
                # We've already created a Signal object for this button signal.
                return self.buttonSignals[name]
            elif name in Mouse.buttons:
                sig = ButtonSignal(name, Mouse.buttons[name])
                self.buttonSignals[name] = sig
                return sig
            else:
                # We don't know what this signal is.
                print "Warning: Unknown mouse signal: " + name
                return None

        def startButtonPolling(self, callback):
            self.pollCallback = callback

            if self.signalsEnabled:
                raise "Error: " + str(self) + " tried to start polling with signals enabled!"
            if self.polling:
                raise "Error: " + str(self) + " already polling for buttons!"

            self.polling = True

            for button, name in Mouse.buttons.iteritems():
                base.accept(button, self.capture, [button, name])

        def stopButtonPolling(self):
            if self.polling:
                for button in Mouse.buttons:
                    base.ignore(button)
                self.polling = False
            else:
                print "Warning: " + str(self) + " tried to stop polling without starting first."

        def capture(self, buttonName):
            print buttonName
            self.stopButtonPolling()
            if self.pollCallback is not None:
                self.pollCallback(self.getSignal(buttonName))

        def enableSignals(self):
            if self.enabled:
                if self.polling:
                    raise "Error: " + str(self) + " tried to enable signals while polling!"

                self.signalsEnabled = True

                # Re-center cursors so we don't get spurious mouse movement.
                center = (base.win.getXSize() / 2, base.win.getYSize() / 2)
                base.win.movePointer(self.mouseIndex, *center)

                for name, signal in self.buttonSignals.iteritems():
                    base.accept(self.mousePrefix + name, signal, [KeyState.Press])
                    base.accept(self.mousePrefix + name + '-up', signal, [KeyState.Release])

                # Hide the mouse cursor.
                props = WindowProperties()
                props.setCursorHidden(True)
                props.setMouseMode(WindowProperties.MRelative)
                base.win.requestProperties(props)

        def disableSignals(self):
            if self.signalsEnabled:
                for name, signal in self.buttonSignals.iteritems():
                    base.ignore(self.mousePrefix + name)
                    base.ignore(self.mousePrefix + name + '-up')
                self.signalsEnabled = False
            else:
                print "Warning: " + str(self) + " tried to disable signals when they weren't enabled."

            # Zero the axes so we don't spin infinitely after disabling.
            self.axisSignals['x'](0)
            self.axisSignals['y'](0)

            # Show the mouse cursor.
            props = WindowProperties()
            props.setCursorHidden(False)
            props.setMouseMode(WindowProperties.MAbsolute)
            base.win.requestProperties(props)

        def printInfo(self):
            "Print out useful information about the keyboard. (not much here)"
            pass

        def enable(self):
            self.enabled = True

            if self.checkAxisTask is None:
                self.checkAxisTask = taskMgr.add(self.tskCheckAxis, "Send events when the mouse position changes.")

            self.enableSignals()

        def disable(self):
            self.enabled = False

            if self.checkAxisTask is not None:
                taskMgr.remove(self.checkAxisTask)
                self.checkAxisTask = None

            self.disableSignals()


    def __init__(self):
        self.devices = []
        if len(base.pointerWatcherNodes) > 1:
            # Get mouse devices for all raw mice:
            for rawMouseIndex, mouse in enumerate(base.pointerWatcherNodes[1:]):
                self.devices.append(Mouse.MouseDevice(rawMouseIndex + 1, mouse))
        else:
            # Default to the virtual pointer:
            print "Warning: No raw mice found! You won't be able to bind " + \
                "multiple mice separately."
            self.devices.append(Mouse.MouseDevice(0, base.pointerWatcherNodes[0]))

    def enable(self, value=True, *args, **kwargs):
        if value:
            for mouse in self.devices:
                mouse.enable()

    def disable(self, value=True, *args, **kwargs):
        if value:
            for mouse in self.devices:
                mouse.disable()

    def toggle(self, value=True, *args, **kwargs):
        if value:
            if self.devices[0].enabled:
                for mouse in self.devices:
                    mouse.disable()
            else:
                for mouse in self.devices:
                    mouse.enable()

    def getDevices(self):
        return self.devices

Mouse = Mouse()

