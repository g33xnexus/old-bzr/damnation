#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: Utility classes.                                                   #
#------------------------------------------------------------------------------#

__all__ = ["ColliderUtil"]

from pandac.PandaModules import Vec3, Vec4, Mat4, Point3, Filename

from panda3d.physx import PhysxBoxShapeDesc, \
        PhysxCapsuleShapeDesc, PhysxPlaneShapeDesc, \
        PhysxTriangleMeshShapeDesc, PhysxMeshPool

from configuration import Configuration


class ColliderUtil(object):
    @staticmethod
    def loadCollider(filename):
        mesh = PhysxMeshPool.loadTriangleMesh(Filename(filename))

        shapeDesc = PhysxTriangleMeshShapeDesc()
        shapeDesc.setMesh(mesh)

        return shapeDesc

    @staticmethod
    def createCollider(definition):
        shapeDesc = None

        if 'type' in definition:
            colliderType = definition['type']
            colliderSize = definition['size']
            collideBits = definition.get('collideBits', None)
            categoryBits = definition.get('categoryBits', None)
            geom = None

            if colliderType == "cylinder":
                print "The \"cylinder\" collider type is deprecated! Use \"capsule\" instead. (%s)" % (definition, )
                shapeDesc = PhysxCapsuleShapeDesc()
                shapeDesc.setHeight(colliderSize[0])
                shapeDesc.setRadius(colliderSize[1])

            elif colliderType == "capsule":
                shapeDesc = PhysxCapsuleShapeDesc()
                shapeDesc.setHeight(colliderSize[0])
                shapeDesc.setRadius(colliderSize[1])

            elif colliderType == "cube":
                shapeDesc = PhysxBoxShapeDesc()
                shapeDesc.setDimensions(Vec3(*([colliderSize] * 3)))

            elif colliderType == "box":
                shapeDesc = PhysxBoxShapeDesc()
                shapeDesc.setDimensions(Vec3(*colliderSize))

            elif colliderType == "plane":
                shapeDesc = PhysxPlaneShapeDesc()
                shapeDesc.setPlane(Vec3(*(colliderSize[:3])), colliderSize[3])
                print "Plane collider:", shapeDesc

        return shapeDesc

