#------------------------------------------------------------------------------#
# Requiem for Innocence: Damnation, a 3D Space MMORPG                          #
#                                                                              #
# Copyright (C) 2004-2008 G33X Nexus Entertainment (http://www.g33xnexus.com/) #
#                                                                              #
# This file is part of RFI: Damnation.                                         #
#                                                                              #
#    Damnation is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by      #
#    the Free Software Foundation, either version 3 of the License, or         #
#    (at your option) any later version.                                       #
#                                                                              #
#    Damnation is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#    GNU General Public License for more details.                              #
#                                                                              #
#    You should have received a copy of the GNU General Public License         #
#    along with Damnation.  If not, see <http://www.gnu.org/licenses/>.        #
#------------------------------------------------------------------------------#

#------------------------------------------------------------------------------#
#    Author: Christopher S. Case <chris.case@g33xnexus.com>                    #
#    Author: David H. Bronke <david.bronke@g33xnexus.com>                      #
#                                                                              #
#    Brief: The window class                                                   #
#------------------------------------------------------------------------------#

# Panda imports
from pandac.PandaModules import WindowProperties

# Python imports
import sys, os, ConfigParser, hashlib, random
from optparse import OptionParser

# Local imports
from configuration import Configuration

class Window(object):
    def __init__(self):
        if Configuration.getBool('startFullscreen'):
            self.setFullscreen()
        else:
            self.setWindowed()

    def setFullscreen(self):
        props = WindowProperties()
        resolution = [int(size) for size in Configuration['fullscreenResolution'].split('x')]
        props.setSize(*resolution)
        props.setFullscreen(True)
        props.setFixedSize(True)
        base.win.requestProperties(props)

    def setWindowed(self):
        props = WindowProperties()
        resolution = [int(size) for size in Configuration['windowedResolution'].split('x')]
        props.setSize(*resolution)
        props.setFullscreen(False)
        props.setFixedSize(True)
        base.win.requestProperties(props)

    def toggleFullscreen(self):
        if base.win.getProperties().getFullscreen():
            self.setWindowed()
        else:
            self.setFullscreen()

Window = Window()

